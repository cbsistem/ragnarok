unit ragnarok.dbsessons;

interface

uses
  System.Types,
  System.Objects,
  System.Widget,
  System.Types.Convert,
  System.Dictionaries,
  System.Time,
  System.Memory.Buffer,

  SmartNJ.Streams,

  Ragnarok.JSON,


  System.db,
  System.dataset,
  SmartNJ.Sqlite3;

type

  TQTXUserSession     = class;
  TQTXSessionManager  = class;

  TQTXAccountPrivilege  =  (
    apExecute,  // user can execute applications
    apRead,     // user can read files
    apWrite,    // user can write files
    apCreate,   // user can create files
    apInstall,  // user can install new software
    apAppstore  // user has access to software repository [appstore]
    );
  TQTXAccountPrivileges = set of TQTXAccountPrivilege;

  TQTXAccountState = (
    asActive,       // Account is active and authenticated
    asLocked,       // Account is active and authenticated, but locked by administrator
    asUnConfirmed   // Session is created, but user has not yet authenticated [Default state]
    );

  TQTXUserSession = class(TW3OwnedObject)
  private
    FManager:   TQTXSessionManager;
  protected
    function    GetPrivileges: TQTXAccountPrivileges; virtual; abstract;
    procedure   SetPrivileges(NewPrivileges: TQTXAccountPrivileges); virtual; abstract;

    function    GetIdentifier: string; virtual; abstract;
    procedure   SetIdentifier(NewIdentifier: string); virtual; abstract;

    function    GetState: TQTXAccountState; virtual; abstract;
    procedure   SetState(NewState: TQTXAccountState); virtual; abstract;

    function    GetRootFolder: string; virtual; abstract;
    procedure   SetRootFolder(NewRootFolder: string); virtual; abstract;

    function    GetRemoteHost: string; virtual; abstract;
    procedure   SetRemoteHost(NewRemoteHost: string); virtual; abstract;

  protected
    function    GetOwner: TQTXSessionManager; reintroduce;
    function    AcceptOwner(const CandidateObject: TObject): boolean; override;
  public
    property    Owner: TQTXSessionManager read GetOwner;

    property    State: TQTXAccountState read GetState write SetState;
    property    Privileges: TQTXAccountPrivileges read GetPrivileges write SetPrivileges;
    property    Identifier: string read GetIdentifier write SetIdentifier;
    property    RootFolder: string read GetRootFolder write SetRootFolder;
    property    RemoteHost: string read GetRemoteHost write SetRemoteHost;

    constructor Create(const Manager: TQTXSessionManager); reintroduce; virtual;
  end;

  TQTXSessionCB = procedure (TagValue: variant);
  TQTXUserSessionCB = procedure (TagValue: variant; Session: TQTXUserSession; Error: Exception);
  TQTXUserSessionCountCB = procedure (TagValue: variant; Count: integer; Error: Exception);

  TQTXSessionManager = class(TObject)
  public
    procedure   GetSessionByIndex(TagValue: Variant; Index: integer; CB: TQTXUserSessionCB); virtual; abstract;
    procedure   GetSessionCount(TagValue: variant; CB: TQTXUserSessionCountCB); virtual; abstract;

    procedure   FindSessionByHost(TagValue: Variant; RemoteHost: string; CB: TQTXUserSessionCB); virtual; abstract;
    procedure   FindSessionbyId(TagValue: Variant; Identifier: string; CB: TQTXUserSessionCB); virtual; abstract;

    procedure   &Register(TagValue: variant; RemoteHost: string; CB: TQTXUserSessionCB); overload; virtual; abstract;
    procedure   &Register(TagValue: Variant; ThisSession: TQTXUserSession; CB: TQTXUserSessionCB); overload; virtual; abstract;

    procedure   FlushSessions(TagValue: variant; CB: TQTXSessionCB); virtual; abstract;
  end;

  TQTXUserSessionMemory = class(TQTXUserSession)
  private
    FPrivileges:  TQTXAccountPrivileges;
    FState:       TQTXAccountState;
    FRootFolder:  string;
    FIdent:       string;
    FRemote:      string;
  protected
    function    GetPrivileges: TQTXAccountPrivileges; override;
    procedure   SetPrivileges(NewPrivileges: TQTXAccountPrivileges); override;

    function    GetIdentifier: string;override;
    procedure   SetIdentifier(NewIdentifier: string); override;

    function    GetState: TQTXAccountState; override;
    procedure   SetState(NewState: TQTXAccountState); override;

    function    GetRootFolder: string; override;
    procedure   SetRootFolder(NewRootFolder: string); override;

    function    GetRemoteHost: string;  override;
    procedure   SetRemoteHost(NewRemoteHost: string);  override;
  end;

  TQTXSessionManagerMemory = class(TQTXSessionManager)
  private
    FSessions:  array of TQTXUserSession;
    FIPLookup:  TW3ObjDictionary;
    FLookup:    TW3ObjDictionary;
  public
    procedure   GetSessionByIndex(TagValue: Variant; Index: integer; CB: TQTXUserSessionCB); override;
    procedure   GetSessionCount(TagValue: variant; CB: TQTXUserSessionCountCB); override;

    procedure   FindSessionByHost(TagValue: Variant; RemoteHost: string; CB: TQTXUserSessionCB); override;
    procedure   FindSessionById(TagValue: Variant; Identifier: string; CB: TQTXUserSessionCB); override;

    procedure   &Register(TagValue: variant; RemoteHost: string; CB: TQTXUserSessionCB); overload; override;
    procedure   &Register(TagValue: Variant; ThisSession: TQTXUserSession; CB: TQTXUserSessionCB); overload; override;

    procedure   FlushSessions(TagValue: variant; CB: TQTXSessionCB); override;

    constructor Create; virtual;
    destructor  Destroy; override;

  end;


implementation

//#############################################################################
// TQTXUserSession
//#############################################################################

constructor TQTXUserSession.Create(const Manager: TQTXSessionManager);
begin
  inherited Create(Manager);
end;

function TQTXUserSession.GetOwner: TQTXSessionManager;
begin
  result := FManager;
end;

function TQTXUserSession.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := (CandidateObject <> nil) and (CandidateObject is TQTXUserSession);
end;

//#############################################################################
// TQTXUserSessionMemory
//#############################################################################

function TQTXUserSessionMemory.GetRemoteHost: string;
begin
  result := FRemote;
end;

procedure TQTXUserSessionMemory.SetRemoteHost(NewRemoteHost: string);
begin
  FRemote := NewRemoteHost;
end;

function TQTXUserSessionMemory.GetPrivileges: TQTXAccountPrivileges;
begin
  result := FPrivileges;
end;

procedure TQTXUserSessionMemory.SetPrivileges(NewPrivileges: TQTXAccountPrivileges);
begin
  FPrivileges := NewPrivileges;
end;

function TQTXUserSessionMemory.GetIdentifier: string;
begin
  result := FIdent;
end;

procedure TQTXUserSessionMemory.SetIdentifier(NewIdentifier: string);
begin
  FIdent := NewIdentifier;
end;

function TQTXUserSessionMemory.GetState: TQTXAccountState;
begin
  result := FState;
end;

procedure TQTXUserSessionMemory.SetState(NewState: TQTXAccountState);
begin
  FState := NewState;
end;

function TQTXUserSessionMemory.GetRootFolder: string;
begin
  result := FRootFolder;
end;

procedure TQTXUserSessionMemory.SetRootFolder(NewRootFolder: string);
begin
  FRootFolder := NewRootFolder;
end;

//#############################################################################
// TQTXSessionManagerMemory
//#############################################################################

constructor TQTXSessionManagerMemory.Create;
begin
  inherited Create;
  FLookup := TW3ObjDictionary.Create;
  FIPLookup := TW3ObjDictionary.Create;
end;

destructor TQTXSessionManagerMemory.Destroy;
begin
  if FSessions.Count > 0 then
    FlushSessions(unassigned, nil);
  FLookup.free;
  FIPLookup.free;
  inherited;
end;

procedure TQTXSessionManagerMemory.FlushSessions(TagValue: variant; CB: TQTXSessionCB);
begin
  try
    for var item in FSessions do
    begin
      Item.free;
    end;
  finally
    FSessions.Clear();
    FLookup.Clear();

    if assigned(CB) then
      CB(TagValue);
  end;
end;

procedure TQTXSessionManagerMemory.&Register(TagValue: variant; RemoteHost: string; CB: TQTXUserSessionCB);
begin
  RemoteHost := RemoteHost.Trim().ToLower();

  if RemoteHost.Length < 1 then
  begin
    if assigned(CB) then
      CB(TagValue, nil, EW3Exception.Create('Failed to add session, ip length was null'));
  end;

  // Do we already have a session for this IP?
  // Look-up the remote host first
  if FIPLookup.Contains(RemoteHost) then
  begin
    // Yes we do, ok return existing session object
    // instead of making a new one
    if assigned(CB) then
      CB(TagValue, TQTXUserSession(FIPLookup[RemoteHost]), nil);
    exit;
  end;

  // No Session exists for this one, so create it
  var LSession := TQTXUserSessionMemory.Create(self);
  LSession.RemoteHost := RemoteHost.trim().ToLower();

  // Add to object array
  var LSessionID := LSession.Identifier.ToLower();
  FSessions.Add(LSession);

  // Add to ID lookup
  FLookup.Values[LSessionID] := LSession;

  //Add to IP lookup
  FIPLookup.Values[RemoteHost] := LSession;
end;

procedure TQTXSessionManagerMemory.&Register(TagValue: Variant; ThisSession: TQTXUserSession; CB: TQTXUserSessionCB);
begin
  if ThisSession <> nil then
  begin
    // If the session has a remote IP, is that already known?
    var LRemoteIP := ThisSession.RemoteHost;
    if LRemoteIP.length > 0 then
    begin
      // It was, so just exit, we have already returned it (see above);
      if FIPLookup.Contains(LRemoteIP) then
      begin
        if assigned(CB) then
          CB(TagValue, ThisSession, NIL);
        exit;
      end;
    end;

    // Check if this is an unknown, new session
    var LSessionID := ThisSession.Identifier.Trim().ToLower();
    if not FLookup.Contains(LSessionID) then
    begin
      try
        // Add to normal array
        FSessions.Add(ThisSession);

        // Add to ID lookup
        FLookup.Values[LSessionID] := ThisSession;
        //SignalSessionOpened(ThisSession);

          //Add to IP lookup (if IP has been set)
        if LRemoteIP.length > 0 then
          FIPLookup.Values[LRemoteIP] := ThisSession;
      finally
        if assigned(CB) then
          CB(TagValue, ThisSession, nil);
      end;
    end;
  end else
  begin
    var LError := EW3Exception.Create('A session with that Identifier already exists error');
    if assigned(CB) then
      CB(TagValue, nil, LError)
    else
      raise LError;
  end;
end;

procedure TQTXSessionManagerMemory.GetSessionByIndex(TagValue: Variant; Index: integer; CB: TQTXUserSessionCB);
var
  LObj: TQTXUserSessionMemory;
begin

  try
    LObj := TQTXUserSessionMemory(FSessions[Index]);

    if assigned(CB) then
      CB(TagValue, LObj, nil);

  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, nil, e)
      else
        raise;
    end;
  end;

end;

procedure TQTXSessionManagerMemory.GetSessionCount(TagValue: variant; CB: TQTXUserSessionCountCB);
begin
  if assigned(CB) then
    CB(TagValue, FSessions.Count, nil);
end;

procedure TQTXSessionManagerMemory.FindSessionByHost(TagValue: Variant; RemoteHost: string; CB: TQTXUserSessionCB);
var
  LObj: TQTXUserSession;
begin
  RemoteHost := RemoteHost.Trim().ToLower();

  if FIPLookup.Contains(RemoteHost) then
  begin
    LObj := TQTXUserSession(FIPLookup.Values[RemoteHost]);
    if assigned(CB) then
      CB(TagValue, LObj, NIL);
  end else
  begin
    var LError := EW3Exception.CreateFmt('Unknown ip [%s], could not locate session error', [RemoteHost]);
    if assigned(CB) then
      CB(TagValue, nil, LError)
    else
      raise LError;
  end;
end;

procedure TQTXSessionManagerMemory.FindSessionbyId(TagValue: Variant; Identifier: string; CB: TQTXUserSessionCB);
begin
  Identifier := Identifier.Trim().ToLower();

  if FLookup.Contains(Identifier) then
  begin
    var LObj := TQTXUserSession( FLookup.Values[Identifier] );
    if assigned(CB) then
      CB(TagValue, LObj, NIL);
  end else
  begin
    var LError := EW3Exception.CreateFmt('Unknown session id [%s], could  not locate session error', [Identifier]);
    if assigned(CB) then
      CB(TagValue, nil, LError)
    else
      raise LError;
  end;
end;


end.
