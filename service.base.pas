unit service.base;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Objects,
  System.Inifile,

  System.db,
  //System.dataset,
  SmartNJ.Sqlite3,

  qtx.logfile,

  ragnarok.types,
  ragnarok.json,
  ragnarok.Server,
  ragnarok.Sessions,
  ragnarok.dbsessons,
  ragnarok.messages.base,
  //ragnarok.messages.network,
  ragnarok.messages.factory,

  service.dispatcher,

  SmartNJ.Server.WebSocket,
  SmartNJ.Server.WebSocketSecure,

  SmartNJ.System,
  SmartNJ.Application,
  SmartNJ.Server.Http;

type

  TRagnarokServerType = (
    rsBasic,    // HTTP based Websocket
    rsSecure    // HTTPS based websocket
    );

  // Callback method typed
  TRagnarokServerStartCB  = procedure (Error: Exception);
  TRagnarokServerStopCB   = procedure (Error: Exception);
  TRagnarokServiceCB      = procedure (Error: Exception);

  TRagnarokServiceFactory = partial class(TMessageFactory)
  protected
    procedure   RegisterIntrinsic; override;
  end;

  TRagnarokService = class(TQTXLogObject)
  private
    FPort:      integer;
    FPrefsName: string = 'preferences.ini';
    FServer:    TNJWebSocketHybridServer;
    FFactory:   TRagnarokServiceFactory;
    FDispatch:  TQTXMessageDispatcher;
  protected
    function    GetActive: boolean;
    procedure   SetPort(const NewPort: integer);

    // These send standard error messages. These are helper
    // functions that just makes things easier when using Ragnarok
    procedure   SendRejection(const Socket: TNJWebSocketSocket; Ticket: string; const Cause: string; const Close: boolean);
    procedure   SendUnSupported(const Socket: TNJWebSocketSocket; Ticket: string);
    procedure   SendInternalError(const Socket: TNJWebSocketSocket; Ticket: string; const Cause: string; const Close: boolean);

    // Message handler, messages are parsed here
    procedure   HandleMessage(Sender: TObject; Socket: TNJWebSocketSocket; Info: TNJWebsocketMessage);

    // Dispatcher, messages are invoked via registered message->id->handler lookup
    procedure   Dispatch(Socket: TNJWebSocketSocket; Message: TQTXBaseMessage); virtual;

    // Mapped event-handlers from the server instance (FServer above)
    procedure   HandleBeforeServerStarted(Sender: TObject); virtual;
    procedure   HandleAfterServerStarted(Sender: TObject); virtual;
    procedure   HandleBeforeServerStopped(Sender: TObject); virtual;
    procedure   HandleAfterServerStopped(Sender: TObject); virtual;

    // Our local variations of standrd state handling
    procedure   BeforeServerStarted; virtual;
    procedure   AfterServerStarted; virtual;
    procedure   BeforeServerStopped; virtual;
    procedure   AfterServerStopped; virtual;

  public
    property    Active: boolean read GetActive;
    property    PreferencesFileName: string read FPrefsName write FPrefsName;
    property    Port: integer read FPort write SetPort;
    property    Server: TNJWebSocketHybridServer read FServer;
    property    MessageFactory: TRagnarokServiceFactory read FFactory;
    property    MessageDispatch: TQTXMessageDispatcher read FDispatch;

    property    OnBeforeServerStarted: TNotifyevent;
    property    OnAfterServerStarted: TNotifyevent;
    property    OnBeforeServerStopped: TNotifyevent;
    property    OnAfterServerStopped: TNotifyevent;

    procedure   Start(const ServerType: TRagnarokServerType; const CB: TRagnarokServerStartCB); virtual;
    procedure   Stop(const CB: TRagnarokServerStopCB); virtual;


    constructor Create; override;
    destructor  Destroy; override;
  end;


implementation

//#############################################################################
// TRagnarokServiceFactory
//#############################################################################

procedure TRagnarokServiceFactory.RegisterIntrinsic;
begin
end;

//#############################################################################
// TRagnarokService
//#############################################################################

constructor TRagnarokService.Create;
begin
  inherited Create;
  FDispatch := TQTXMessageDispatcher.Create();
  FPort := 1881;
end;

destructor TRagnarokService.Destroy;
begin
  if FServer <> nil then
  begin
    FServer.free;
    FServer := nil;
  end;
  FFactory.free;
  FDispatch.free;
  inherited;
end;


procedure TRagnarokService.SendInternalError(const Socket: TNJWebSocketSocket;
  Ticket: string; const Cause: string; const Close: boolean);
begin
  if Socket <> nil then
  begin
    // Internal error
    var reply := TQTXErrorMessage.Create(Ticket);
    try
      reply.Code := CNT_MESSAGE_CODE_ERROR;
      reply.Response := format(CNT_MESSAGE_TEXT_ERROR, [Cause]);

      if Socket.ReadyState = rsOpen then
      begin
        Socket.Send(reply.Serialize());
        if Close then
          Socket.Close();
      end;
    finally
      reply.free;
    end;
  end;
end;

procedure TRagnarokService.SendRejection(const Socket: TNJWebSocketSocket;
  Ticket: string; const Cause: string; const Close: boolean);
begin
  if Socket <> nil then
  begin
    // Unknown or unsupported identifier, return error
    var reply := TQTXErrorMessage.Create(Ticket);
    try
      reply.Code := CNT_MESSAGE_CODE_REJECTED;
      reply.Response := Format(CNT_MESSAGE_TEXT_REJECTED, [Cause]);

      if Socket.ReadyState = rsOpen then
      begin
        Socket.Send(Reply.Serialize());
        if Close then
          Socket.Close();
      end;
    finally
      reply.free;
    end;
  end;
end;

procedure TRagnarokService.SendUnSupported(const Socket: TNJWebSocketSocket; Ticket: string);
begin
  if Socket <> nil then
  begin
    // Unknown or unsupported identifier, return error
    var reply := TQTXErrorMessage.Create(Ticket);
    try
      reply.Code := CNT_MESSAGE_CODE_ERROR;
      reply.Response := CNT_MESSAGE_TEXT_ERROR;

      if Socket.ReadyState = rsOpen then
        Socket.Send( Reply.Serialize() );
    finally
      reply.free;
    end;
  end;
end;

procedure TRagnarokService.HandleMessage(Sender: TObject; Socket: TNJWebSocketSocket; Info: TNJWebsocketMessage);
var
  LInstance: TQTXBaseMessage;
begin
  var LId := TQTXBaseMessage.QueryIdentifier(Info.wiText);
  if FFactory.Build(LId, LInstance) then
    Dispatch(Socket, LInstance)
  else
    raise Exception.Create('Unknown identifier [' + LId + '] or message format error');
end;

procedure TRagnarokService.Dispatch(Socket: TNJWebSocketSocket; Message: TQTXBaseMessage);
begin
end;

function TRagnarokService.GetActive: boolean;
begin
  result := assigned(FServer) and FServer.Active;
end;

procedure TRagnarokService.SetPort(const NewPort: integer);
begin
  if NewPort <> FPort then
  begin
    if FServer <> nil then
    begin
      if not FServer.Active then
        FPort := NewPort
      else
        raise Exception.Create('Port cannot be altered while server is active errror');
    end else
    FPort := NewPort;
  end;
end;

procedure TRagnarokService.HandleBeforeServerStarted(Sender: TObject);
begin
  BeforeServerStarted();
end;

procedure TRagnarokService.HandleAfterServerStarted(Sender: TObject);
begin
  AfterServerStarted();
end;

procedure TRagnarokService.HandleBeforeServerStopped(Sender: TObject);
begin
  BeforeServerStopped();
end;

procedure TRagnarokService.HandleAfterServerStopped(Sender: TObject);
begin
  AfterServerStopped();
end;

procedure TRagnarokService.Start(const ServerType: TRagnarokServerType; const CB: TRagnarokServerStartCB);
begin
  // Make sure server is not already running
  if GetActive() then
  begin
    var Err := Exception.Create("failed to start server, server is already active error");
    if assigned(CB) then
    begin
      CB(err);
      exit;
    end else
    raise Err;
  end;

  // Create the server-type in question
  try
    case ServerType of
    rsBasic:  FServer := TNJWebSocketServer.Create();
    rsSecure: FServer := TNJWebSocketServerSecure.Create();
    end;
  except
    on e: exception do
    begin
      FServer := nil;
      if assigned(CB) then
      begin
        CB(e);
        exit;
      end else
      raise e;
    end;
  end;

  // Assign the port
  FServer.Port := FPort;

  // Create our factory
  FFactory := TRagnarokServiceFactory.Create();

  // Map events so they are replicated in this instance
  FServer.OnBeforeServerStarted := @HandleBeforeServerStarted;
  FServer.OnBeforeServerStopped := @HandleBeforeServerStopped;
  FServer.OnAfterServerStarted  := @HandleAfterServerStarted;
  FServer.OnAfterServerStopped := @HandleAfterServerStopped;

  // Setup standard settings
  FServer.ClientTracking := true;
  FServer.OnTextMessage := @HandleMessage;

  // Fire up the server
  try
    FServer.Start();
  except
    on e: exception do
    begin
      var Err := Exception.Create('An error occured starting the server: ' + e.message);
      if assigned(CB) then
      begin
        CB(Err);
        exit;
      end else
      raise err;
    end;
  end;

  // Everything ok? Great, fire the callback with no errors
  if assigned(CB) then
    CB(nil);
end;

procedure TRagnarokService.Stop(const CB: TRagnarokServerStopCB);
begin
  // Make sure we have an active instance
  if not GetActive() then
  begin
      var Err := Exception.Create('Failed to stop server, no instance is active error');
      if assigned(CB) then
      begin
        CB(Err);
        exit;
      end else
      raise Err;
  end;

  // Shut down, sink any exceptions
  try
    FServer.Stop();
  except
    on e: exception do;
  end;

  // Dispose of the instance, just sink any errors
  try
    try
      FServer.free;
    finally
      FServer := nil;
    end;
  except
    on e: exception do;
  end;

  // Kill the factory
  if FFactory <> nil then
  begin
    try
      try
        FFactory.free;
      finally
        FFactory := nil;
      end;
    except
      on e: exception do;
    end;
  end;

  // OK, nothing wrong -- fire the callback with no errors
  if assigned(CB) then
    CB(nil);
end;

procedure TRagnarokService.BeforeServerStarted;
begin
  if assigned(OnBeforeServerStarted) then
    OnBeforeServerStarted(self);
end;

procedure TRagnarokService.AfterServerStarted;
begin
  if assigned(OnAfterServerStarted) then
    OnAfterServerStarted(self);
end;

procedure TRagnarokService.BeforeServerStopped;
begin
  if assigned(OnBeforeServerStopped) then
    OnBeforeServerStopped(self);
end;

procedure TRagnarokService.AfterServerStopped;
begin
  if assigned(OnAfterServerStopped) then
    OnAfterServerStopped(self);
end;

end.
