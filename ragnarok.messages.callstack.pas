unit ragnarok.messages.callstack;

interface

uses
  System.Types,
  System.Dictionaries,
  ragnarok.messages.base;

type
  // Forward declarations
  TWbCallStack  = class;
  TWbCallTicket = class;

  // Exceptions
  EWbCallStack  = class(EW3Exception);

  // Events and callbacks
  TWbCStackTicketHandler = procedure (const Response: TQTXBaseMessage);
  TWbCStackCommitEvent   = procedure (Sender: TWbCallStack; Ticket: TWbCallTicket);
  TWbCStackInvokeEvent   = procedure (Sender: TWbCallStack; Ticket: TWbCallTicket);
  TWbCStackRevokeEvent   = procedure (Sender: TWbCallStack; Ticket: TWbCallTicket);

  // Ticket object
  TWbCallTicket = class(TObject)
  public
    property Ticket: string;
    property Handler: TWbCStackTicketHandler;
  end;

  TWbCallStack = class(TObject)
  private
    FCallStack:   TW3ObjDictionary;
  public
    property    Count: integer read ( FCallstack.GetKeyCount() );

    // Before sending, a ticket must be commited to the stack.
    // A ticket holds the ticket-id and a handler entrypoint.
    procedure   Commit(Ticket: string; CB: TWbCStackTicketHandler); overload;
    procedure   Commit(const TicketInstance: TWbCallTicket); overload;

    // When a response is received, the ticket is used to invoke the
    // handler associated with the ticket.
    // **NOTE: This also removes the ticket from the stack.
    procedure   Invoke(Ticket: string; Response: TQTXBaseMessage); overload;
    procedure   Invoke(Ticket: string); overload;

    // A registered ticket can be removed by revoking it.
    // This releases the ticket and the stack retains no knowledge
    // of the initial commit
    procedure   Revoke(Ticket: string);

    // This function makes it easy to check if a ticket exist in the stack
    function    Contains(Ticket: string): boolean;

    // Audit enumerates the tickets and returns the ticket identifiers
    // as a normal array of string
    function    Audit: TStrArray;

    // Releases all ticket objects in the stack.
    // This is the same as revoking all commits to the stack.
    procedure   Clear;

    constructor Create; virtual;
    destructor  Destroy; override;
  published
    property  OnTicketCommited: TWbCStackCommitEvent;
    property  OnTicketInvoked: TWbCStackInvokeEvent;
    property  OnTicketRevoked: TWbCStackRevokeEvent;
  end;


implementation

//#############################################################################
// TWbCallStack
//#############################################################################

constructor TWbCallStack.Create;
begin
  inherited Create;
  FCallStack := TW3ObjDictionary.Create();
end;

destructor TWbCallStack.Destroy;
begin
  Clear();
  FCallStack.free;
  inherited;
end;

function TWbCallStack.Audit: TStrArray;
begin
  var LCache: TStrArray;

  FCallstack.ForEach(
    function (ItemKey: string; var KeyData: variant): TEnumResult
    begin
      var LTicket := TWbCallTicket( TVariant.AsObject(KeyData) );
      LCache.add(LTicket.Ticket);
      result := TEnumResult.erContinue;
    end
  );

  result := LCache;
end;

procedure TWbCallStack.Clear;
begin
  try
    FCallstack.ForEach(
      function (ItemKey: string; var KeyData: variant): TEnumResult
      begin
        var LTicket := TWbCallTicket( TVariant.AsObject(KeyData) );

        if assigned(OnTicketRevoked) then
          OnTicketRevoked(self, LTicket);

        LTicket.free;
        KeyData := nil;
        result := TEnumResult.erContinue;
      end
    );
  finally
    FCallstack.Clear();
  end;
end;

function TWbCallStack.Contains(Ticket: string): boolean;
begin
  Ticket := Ticket.trim().ToLower();
  if Ticket.length > 0 then
    result := FCallstack.Contains(Ticket);
end;

procedure TWbCallStack.Revoke(Ticket: string);
begin
  Ticket := Ticket.trim().ToLower();
  if Ticket.length > 0 then
  begin
    if FCallstack.Contains(Ticket) then
    begin
      var LTicket := TWbCallTicket( FCallstack.Values[Ticket] );
      try

        if assigned(OnTicketRevoked) then
          OnTicketRevoked(self, LTicket);

      finally
        LTicket.free;
        FCallstack.Delete(Ticket);
      end;
    end else
      raise EWbCallStack.CreateFmt('Failed to revoke ticket [%s], not in callstack error', [Ticket]);
  end else
  raise EWbCallStack.Create('Failed to revoke ticket, invalid ticket identifier error');
end;

procedure TWbCallStack.Commit(const TicketInstance: TWbCallTicket);
begin
  if TicketInstance <> nil then
  begin
    TicketInstance.Ticket := TicketInstance.Ticket.Trim().ToLower();
    if not Contains(TicketInstance.Ticket) then
    begin
      FCallStack.Values[TicketInstance.Ticket] := TicketInstance;

      if assigned(OnTicketCommited) then
        OnTicketCommited(self, TicketInstance);
    end else
    raise EWbCallStack.CreateFmt
    ('Failed to commit ticket [%s], already in callstack error', [TicketInstance.Ticket]);
  end else
  raise EWbCallStack.Create('Failed to commit ticket, instance was nil error');
end;

procedure TWbCallStack.Commit(Ticket: string; CB: TWbCStackTicketHandler);
begin
  Ticket := Ticket.trim().ToLower();
  if Ticket.length > 0 then
  begin
    if not FCallstack.Contains(Ticket) then
    begin
      // Setup a ticket object
      var CacheObj := TWbCallTicket.Create;
      CacheObj.Ticket := Ticket;
      CacheObj.Handler := @CB;

      FCallStack.Values[Ticket] := CacheObj;

      if assigned(OnTicketCommited) then
        OnTicketCommited(self, CacheObj);

    end else
    raise EWbCallStack.CreateFmt('Failed to commit ticket [%s], already in callstack error', [Ticket]);
  end else
  raise EWbCallStack.Create('Failed to commit ticket, invalid ticket identifier error');
end;

procedure TWbCallStack.Invoke(Ticket: string);
begin
  Ticket := Ticket.trim().ToLower();
  if Ticket.length > 0 then
  begin
    if FCallstack.Contains(Ticket) then
    begin
      var LCacheObj := TWbCallTicket( FCallstack.Values[ticket] );
      try
        if assigned(LCacheObj.Handler) then
          LCacheObj.Handler(nil);
      finally
        if assigned(OnTicketInvoked) then
          OnTicketInvoked(self, LCacheObj);

        LCacheObj.free;
        FCallstack.Delete(Ticket);
      end;
    end else
    raise EWbCallStack.CreateFmt('Failed to invoke ticket [%s], not in callstack error', [Ticket]);
  end else
  raise EWbCallStack.Create('Failed to invoke ticket, invalid ticket identifier error');
end;

procedure TWbCallStack.Invoke(Ticket: string; Response: TQTXBaseMessage);
begin
  Ticket := Ticket.trim().ToLower();
  if Ticket.length > 0 then
  begin
    if FCallstack.Contains(Ticket) then
    begin
      var LCacheObj := TWbCallTicket( FCallstack.Values[ticket] );
      try
        if assigned(LCacheObj.Handler) then
          LCacheObj.Handler(Response);
      finally
        if assigned(OnTicketInvoked) then
          OnTicketInvoked(self, LCacheObj);

        LCacheObj.free;
        FCallstack.Delete(Ticket);
      end;
    end else
    raise EWbCallStack.CreateFmt('Failed to invoke ticket [%s], not in callstack error', [Ticket]);
  end else
  raise EWbCallStack.Create('Failed to invoke ticket, invalid ticket identifier error');
end;


end.
