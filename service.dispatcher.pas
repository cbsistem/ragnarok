unit service.dispatcher;

interface

uses
  System.Types,
  System.Objects,
  System.Types.Convert,
  System.Time,
  System.Dictionaries,

  ragnarok.json,
  ragnarok.messages.base,

  SmartNJ.Server.WebSocket,

  System.Streams;

type

  // Exceptions
  EQTXMessageDispatcher  = class(EW3Exception);

  // Forward declarations
  TQTXMessageInfo  = class;

  TQTXDispatchHandler = procedure (Socket: TNJWebSocketSocket; Request: TQTXBaseMessage);

  TQTXMessageInfo = class
    MessageClass:   TQTXMessageBaseClass;
    MessageHandler: TQTXDispatchHandler;
  end;

  TQTXMessageDispatcher = class(TObject)
  private
    FItems:     array of TQTXMessageInfo;
    FNameLUT:   TW3ObjDictionary;
  public
    property    Items[const index: integer]: TQTXMessageInfo read ( FItems[index] ); default;
    property    Count: integer read (FItems.Count);

    function    GetMessageInfoForName(MessageClassName: string): TQTXMessageInfo;
    function    GetMessageInfoForClass(const MessageClass: TQTXMessageBaseClass): TQTXMessageInfo;
    procedure   RegisterMessage(const MessageClass: TQTXMessageBaseClass; const Handler: TQTXDispatchHandler);

    procedure   Clear; virtual;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;


implementation


resourcestring
  CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_CLASSNIL =
  'Failed to register message, classtype was nil error';

  CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLERNIL =
  'Failed to register message, handler as nil error';

  CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLEREXISTS =
  'Failed to register message, a handler for that message already exists error';

//#############################################################################
// TQTXMessageDispatcher
//#############################################################################

constructor TQTXMessageDispatcher.Create;
begin
  inherited Create;
  FNameLUT := TW3ObjDictionary.Create;
  //FIDLUT := TW3ObjDictionary.Create;
end;

destructor TQTXMessageDispatcher.Destroy;
begin
  if FItems.Count > 0 then
    Clear();
  FNameLut.free;
  //FIdLUT.free;
  inherited;
end;

procedure TQTXMessageDispatcher.Clear;
begin
  try
    for var info in FItems do
    begin
      info.free;
    end;
  finally
    FItems.clear();
    FNameLUT.clear();
    //FIdLUT.clear();
  end;
end;

function TQTXMessageDispatcher.GetMessageInfoForClass
  (const MessageClass: TQTXMessageBaseClass): TQTXMessageInfo;
begin
  if MessageClass <> nil then
  begin
    for var Info in FItems do
    begin
      if Info.MessageClass = MessageClass then
      begin
        result := info;
        break;
      end;
    end;
  end;
end;

function TQTXMessageDispatcher.GetMessageInfoForName
  (MessageClassName: string): TQTXMessageInfo;
begin
  MessageClassName := MessageClassName.Trim().ToLower();
  if MessageClassName.Length > 0 then
  begin
    if FNameLUT.Contains(MessageClassName) then
      result := TQTXMessageInfo(FNameLUT.Values[MessageClassName]);
  end;
end;

procedure TQTXMessageDispatcher.RegisterMessage
  (const MessageClass: TQTXMessageBaseClass; const Handler: TQTXDispatchHandler);
begin
  if MessageClass <> nil then
  begin
    if assigned(Handler) then
    begin
      var LName := MessageClass.ClassName.Trim().ToLower();
      if not FNameLUT.Contains(LName) then
      begin
        // Create message handler info
        var Info := TQTXMessageInfo.Create;
        Info.MessageClass := MessageClass;
        Info.MessageHandler := @Handler;

        // Add to linear-list
        FItems.Add(Info);

        // Register by name in our string->object lookup table
        FNameLUT.Values[LName] := Info;

      end else
      raise EQTXMessageDispatcher.Create(CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLEREXISTS);
    end else
    raise EQTXMessageDispatcher.Create(CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLERNIL);
  end else
  raise EQTXMessageDispatcher.Create(CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_CLASSNIL);
end;


end.

