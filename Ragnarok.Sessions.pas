//#############################################################################
// ______                                  _
// | ___ \ Smart Mobile Studio            | |
// | |_/ /__ _  __ _ _ __   __ _ _ __ ___ | | __
// |    // _` |/ _` | '_ \ / _` | '__/ _ \| |/ /
// | |\ \ (_| | (_| | | | | (_| | | | (_) |   <
// \_| \_\__,_|\__, |_| |_|\__,_|_|  \___/|_|\_\
//              __/ |
//             |___/          Powered by Node.js
//
//#############################################################################

unit Ragnarok.Sessions;

interface

uses
  System.Types,
  System.Objects,
  System.Widget,
  System.Types.Convert,
  System.Dictionaries,
  System.Time,
  System.JSON,
  System.Streams,
  System.Structure,
  System.Structure.JSON;

type

  // Forward declarations
  TUserSession  = partial class (TW3OwnedErrorObject);
  TUserSessionManager = partial class (TW3ErrorObject);

  TWbAccountPrivilege  =  (
    apExecute,  // user can execute applications
    apRead,     // user can read files
    apWrite,    // user can write files
    apCreate,   // user can create files
    apInstall,  // user can install new software
    apAppstore  // user has access to software repository [appstore]
    );
  TWbAccountPrivileges = set of TWbAccountPrivilege;

  TWbAccountState = (
    asActive,       // Account is active and authenticated
    asLocked,       // Account is active and authenticated, but locked by administrator
    asUnConfirmed   // Session is created, but user has not yet authenticated [Default state]
    );

  TUserSessionData = record
    Privileges: TWbAccountPrivileges;
    State:      TWbAccountState;
    Identifier: string;
    Username:   string;
    Password:   string;
    LoginTime:  TDateTime;
    RootFolder: string;
    Values:     variant;
  end;

  IUserSession = interface
    ['{e01e3f0b-650d-41e6-a335-361c9311e7c5}']
    function    GetOwner: TUserSessionManager;
    function    GetValues: IW3Structure;

    function    GetIdentifier: string;
    procedure   SetIdentifier(NewIdentifier: string);

    function    GetPrivileges: TWbAccountPrivileges;
    procedure   SetPrivileges(const NewPrivileges: TWbAccountPrivileges);

    function    GetState: TWbAccountState;
    procedure   SetState(const NewState: TWbAccountState);

    function    GetRootFolder: string;
    procedure   SetRootFolder(const NewRootFolder: string);

    procedure   Reset;
    function    ToJSON: string;
    procedure   FromJSON(const Data: string);
  end;

  TUserSession = partial class(TW3OwnedErrorObject, IUserSession)
  private
    FData:      TUserSessionData;
    FRemoteIP:  string;
    FValues:    TJSONStructure;
    function    GetValues: IW3Structure;

    function    GetPrivileges: TWbAccountPrivileges;
    procedure   SetPrivileges(const NewPrivileges: TWbAccountPrivileges);

    function    GetIdentifier: string;
    procedure   SetIdentifier(NewIdentifier: string);

    function    GetState: TWbAccountState;
    procedure   SetState(const NewState: TWbAccountState);

    function    GetRootFolder: string;
    procedure   SetRootFolder(const NewRootFolder: string);

  protected
    function    GetOwner: TUserSessionManager; reintroduce;
    function    AcceptOwner(const CandidateObject: TObject): boolean; override;
  public
    property    Owner: TUserSessionManager read GetOwner;
    property    RemoteIP: string read FRemoteIP;

    // Like you would expect from a session object, you can write and append
    // as much data as you like. We have given it a full TW3Structure instance
    // so that we can serialize these values too (!)
    // Note: if use ToJSON() or FromJSON() the values stored here are
    // included in the serialization (!)
    property    Values: IW3Structure read GetValues;

    property    Privileges: TWbAccountPrivileges read GetPrivileges write SetPrivileges;
    property    State:      TWbAccountState read GetState write SetState;             // account and session state
    property    Identifier: string read GetIdentifier write SetIdentifier;            // GUID identifier issued by server
    property    Username:   string read (FData.Username) write (FData.UserName);      // Username
    property    Password:   string read (FData.Password) write (FData.Password);      // Double hashed password
    property    LoginTime:  TDateTime read (FData.LoginTime) write (FData.LoginTime); // Time when login was granted
    property    RootFolder: string read (FData.RootFolder) write (FData.RootFolder);  // Account data folder [if any]

    procedure   Reset; virtual;

    function    ToJSON: string; virtual;
    procedure   FromJSON(const Data: string); virtual;

    constructor Create(Manager: TUserSessionManager; UserIP: string); reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TUserSessionManagerEvent = procedure (Sender: TObject; Session: TUserSession);
  TUserSessionManagerPrivilegeGranted = procedure (Sender: TObject; Session: TUserSession; Privilege: TWbAccountPrivilege);
  TUserSessionManagerPrivilegeRevoked = procedure (Sender: TObject; Session: TUserSession; Privilege: TWbAccountPrivilege);
  TUserSessionEnumCallback = function (const Session: TUserSession): TEnumResult;

  // ----

  TUserSessionStdCallback   = procedure (Success: boolean; Session: TUserSession);
  TUserSessionCountCallback = procedure (Success: boolean; Count: integer);

  TUserSessionEnumBeforeCallback = procedure (var TagValue: variant);
  TUserSessionEnumBodyCallback  = function (const TagValue: variant; const Session: TUserSession): TEnumResult;
  TUserSessionEnumAfterCallback = procedure (const TagValue: variant);

  TUserSessionManager = partial class(TW3ErrorObject)
  private
    FSessions:  array of TUserSession;
    FIPLookup:  TW3ObjDictionary;
    FLookup:    TW3ObjDictionary;
  protected
    procedure   SessionAdded(const Session: TUserSession);
    procedure   SessionDeleted(const Session: TUserSession);
    procedure   SessionRenamed(const Session: TUserSession; OldName, NewName: string);
    procedure   PrivilegeGranted(Session: TUserSession; Privilege: TWbAccountPrivilege);
    procedure   PrivilegeRevoked(Session: TUserSession; Privilege: TWbAccountPrivilege);
  public
    property    Sessions[const index: integer]: TUserSession read (FSessions[index]); default;
    property    Count: integer read (FSessions.length);

    procedure   ForEach(const Process: TUserSessionEnumCallback); overload;
    procedure   ForEach(const Before: TProcedureRef;
                const Process: TUserSessionEnumCallback;
                const After: TProcedureRef); overload;

    function    GetSessionByIP(RemoteIP: string): TUserSession;
    function    GetSessionbyId(Identifier: string): TUserSession;

    function    Add(FromThisIP: string): TUserSession; overload;
    function    Add(const ThisSession: TUserSession): TUserSession; overload;

    procedure   Clear; virtual;

    constructor Create; override;
    destructor  Destroy; override;

  published
    property  OnSessionOpened: TUserSessionManagerEvent;
    property  OnSessionClosed: TUserSessionManagerEvent;
    property  OnPrivilegeRevoked: TUserSessionManagerPrivilegeRevoked;
    property  OnPrivilegeGranted: TUserSessionManagerPrivilegeGranted;
  end;

function SessionManager: TUserSessionManager;

implementation

var
  __SessionManager: TUserSessionManager;

function SessionManager: TUserSessionManager;
begin
  if __SessionManager = nil then
    __SessionManager := TUserSessionManager.Create;
  result := __SessionManager;
end;

//#############################################################################
// TUserSessionManager
//#############################################################################

constructor TUserSessionManager.Create;
begin
  inherited Create;
  FLookup := TW3ObjDictionary.Create;
  FIPLookup := TW3ObjDictionary.Create;
end;

destructor TUserSessionManager.Destroy;
begin
  if FSessions.Count > 0 then
    Clear();
  FLookup.free;
  FIPLookup.free;
  inherited;
end;

procedure TUserSessionManager.Clear;
begin
  try
    for var item in FSessions do
    begin
      Item.free;
    end;
  finally
    FSessions.Clear();
    FLookup.Clear();
  end;
end;

procedure TUserSessionManager.PrivilegeRevoked(Session: TUserSession; Privilege: TWbAccountPrivilege);
begin
  if Session.State = asActive then
  begin
    if assigned(OnPrivilegeRevoked) then
      OnPrivilegeRevoked(self, Session, Privilege);
  end;
end;

procedure TUserSessionManager.PrivilegeGranted(Session: TUserSession; Privilege: TWbAccountPrivilege);
begin
  if Session.State = asActive then
  begin
    if assigned(OnPrivilegeGranted) then
      OnPrivilegeGranted(self, Session, Privilege);
  end;
end;

procedure TUserSessionManager.SessionAdded(const Session: TUserSession);
begin
  if assigned(OnSessionOpened) then
    OnSessionOpened(self, Session);
end;

procedure TUserSessionManager.SessionDeleted(const Session: TUserSession);
begin
  if Session <> nil then
  begin
    // Remove from internal list
    for var x := 0 to FSessions.Count-1 do
    begin
      if FSessions[x] = Session then
      begin
        FSessions.Delete(x);
        break;
      end;
    end;

    // Remove from lookup-table
    var id := Session.Identifier.ToLower();
    if FLookup.Contains(id) then
    begin
      FLookup.Delete(id);
    end;

    // Remove from IP lookup table
    var LRemoteIP := Session.RemoteIP.trim().ToLower();
    if FIPLookup.Contains(LRemoteIP) then
    begin
      FIPLookup.Delete(LRemoteIP);
    end;

    if assigned(OnSessionClosed) then
      OnSessionClosed(self, Session);
  end;
end;

procedure TUserSessionManager.SessionRenamed(const Session: TUserSession; OldName, NewName: string);
begin
  ClearLastError();
  if Session <> nil then
  begin
    // Remove old registry in lookup table
    OldName := OldName.ToLower().trim();
    if FLookup.Contains(OldName) then
      FLookup.Delete(OldName);

    NewName := NewName.ToLower().Trim();
    if NewName.length > 0 then
      FLookup.Values[NewName] := Session
    else
    SetLastErrorF('Failed to rename session [%s], new identifier was empty error', [OldName]);
  end else
  SetLastErrorF('Failed to rename session [%s], object was NIL error', [OldName]);
end;

function TUserSessionManager.GetSessionByIP(RemoteIP: string): TUserSession;
begin
  RemoteIP := RemoteIP.trim().ToLower();
  if RemoteIP.length > 0 then
  begin
    if FIPLookup.Contains(RemoteIP) then
      result :=  TUserSession( FIPLookup.Values[RemoteIP] )
    else
      result := nil;
  end;
end;

function TUserSessionManager.GetSessionbyId(Identifier: string): TUserSession;
begin
  ClearLastError();
  Identifier := Identifier.ToLower().Trim();
  if Identifier.length > 0 then
  begin
    if FLookup.Contains(Identifier) then
      result := TUserSession( FLookup.Values[Identifier] )
    else
      SetLastErrorF('Uknown session [%s] error', [Identifier]);
  end else
  SetLastError("Invalid session-id error");
end;

function TUserSessionManager.Add(const ThisSession: TUserSession): TUserSession;
begin
  ClearLastError();
  result := ThisSession;
  if ThisSession <> nil then
  begin

    // If the session has a remote IP, is that already known?
    var LRemoteIP := ThisSession.RemoteIP.trim();
    if LRemoteIP.length > 0 then
    begin
      // It was, so just exit, we have already returned it (see above);
      if FIPLookup.Contains(ThisSession.RemoteIP) then
        exit;
    end;

    // Check if this is a unknown, new session
    var LSessionID := ThisSession.Identifier.ToLower();
    if not FLookup.Contains(LSessionID) then
    begin
      // Add to normal array
      FSessions.Add(ThisSession);

      // Add to ID lookup
      FLookup.Values[LSessionID] := ThisSession;

        //Add to IP lookup (if IP has been set)
      if LRemoteIP.length > 0 then
        FIPLookup.Values[LRemoteIP] := result;

      SessionAdded(ThisSession);
    end;
  end else
  SetLastError('Failed to add session, instance was NIL error');
end;

function TUserSessionManager.Add(FromThisIP: string): TUserSession;
begin
  ClearLastError();

  FromThisIP := FromThisIP.Trim().ToLower();

  if FromThisIP.Length < 1 then
    SetLastError('Failed to add session, remote IP was empty error');

  // Do we already have a session for this IP?
  // Look-up the remote host first
  if FIPLookup.Contains(FromThisIP) then
  begin
    // Yes we do, ok return existing session object
    // instead of making a new one
    result := TUserSession( FIPLookup[FromThisIP] );
    exit;
  end;

  // No Session exists for this one, so create it
  result := TUserSession.Create(Self, FromThisIP);
  //result.SetRemoteIP(FromThisIP);

  // Add to object array
  var LSessionID := result.Identifier.ToLower();
  FSessions.Add(result);

  // Add to ID lookup
  FLookup.Values[LSessionID] := result;
  SessionAdded(result);

  //Add to IP lookup
  FIPLookup.Values[FromThisIP] := result;
end;

procedure TUserSessionManager.ForEach(const Process: TUserSessionEnumCallback);
begin
  for var LItem in FSessions do
  begin
    if Process(LItem) = erBreak then
      break;
  end;
end;

procedure TUserSessionManager.ForEach(const Before: TProcedureRef;
          const Process: TUserSessionEnumCallback;
          const After: TProcedureRef);
begin
  try
    if assigned(Before) then
    Before();
  finally
    try
      if assigned(Process) then
      begin
        for var LItem in FSessions do
        begin
          if Process(LItem) = erBreak then
            break;
        end;
      end;
    finally
      if assigned(After) then
        After();
    end;
  end;
end;

//#############################################################################
// TUserSession
//#############################################################################

constructor TUserSession.Create(Manager: TUserSessionManager; UserIP: string);
begin
  inherited Create(Manager);
  FValues := TJSONStructure.Create;
  Reset();
  FRemoteIP := UserIP.trim();
end;

destructor TUserSession.Destroy;
begin
  if Owner <> nil then
    Owner.SessionDeleted(self);
  FValues.free;
  inherited;
end;

procedure TUserSession.Reset;
begin
  FData.Privileges := [];
  FData.State := TWbAccountState.asUnConfirmed;
  FData.Identifier := 'Session' + TW3Identifiers.GenerateUniqueNumber().ToString();
  FData.LoginTime := now;
  FData.Username := '';
  FData.Password := '';
end;

function TUserSession.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := (CandidateObject <> nil) and (CandidateObject is TUserSessionManager);
end;

function TUserSession.GetOwner: TUserSessionManager;
begin
  result := TUserSessionManager( inherited GetOwner() );
end;

function TUserSession.GetValues: IW3Structure;
begin
  result := FValues as IW3Structure;
end;

function TUserSession.GetRootFolder: string;
begin
  result := FData.RootFolder;
end;

procedure TUserSession.SetRootFolder(const NewRootFolder: string);
begin
  FData.RootFolder := NewRootFolder;
end;

function TUserSession.GetIdentifier: string;
begin
  result := FData.Identifier;
end;

procedure TUserSession.SetIdentifier(NewIdentifier: string);
begin
  NewIdentifier := NewIdentifier.trim();
  if NewIdentifier <> FData.Identifier then
  begin
    var old := FData.Identifier;
    FData.Identifier := NewIdentifier;
  end;
end;

function TUserSession.GetState: TWbAccountState;
begin
  result := FData.State;
end;

procedure TUserSession.SetState(const NewState: TWbAccountState);
begin
  if NewState <> FData.State then
  begin
    FData.State := NewState;
  end;
end;

function TUserSession.GetPrivileges: TWbAccountPrivileges;
begin
  result := FData.Privileges;
end;

procedure TUserSession.SetPrivileges(const NewPrivileges: TWbAccountPrivileges);
begin
  (* apExecute granted by server? *)
  if (apExecute in NewPrivileges)
  and not (apExecute in Privileges) then
  begin
    include(FData.Privileges, apExecute);
    Owner.PrivilegeGranted(self, apExecute);
  end;

  (* apExecute revoked by server? *)
  if (apExecute in Privileges)
  and not (apExecute in NewPrivileges) then
  begin
    exclude(FData.Privileges, apExecute);
    Owner.PrivilegeRevoked(self, apExecute);
  end;

  (* Read granted by server? *)
  if (apRead in NewPrivileges)
  and not (apRead in Privileges) then
  begin
    include(FData.Privileges, apRead);
    Owner.PrivilegeGranted(self, apRead);
  end;

  (* Read revoked by server? *)
  if (apRead in Privileges)
  and not (apRead in NewPrivileges) then
  begin
    exclude(FData.Privileges, apRead);
    Owner.PrivilegeRevoked(self, apRead);
  end;

  (* Write granted by server? *)
  if (apWrite in NewPrivileges)
  and not (apWrite in Privileges) then
  begin
    include(FData.Privileges, apWrite);
    Owner.PrivilegeGranted(self, apWrite);
  end;

  (* Write revoked by server? *)
  if (apWrite in Privileges)
  and not (apWrite in NewPrivileges) then
  begin
    exclude(FData.Privileges, apWrite);
    Owner.PrivilegeRevoked(self, apWrite);
  end;

  (* Create granted by server? *)
  if (apCreate in NewPrivileges)
  and not (apCreate in Privileges) then
  begin
    include(FData.Privileges, apCreate);
    Owner.PrivilegeGranted(self, apCreate);
  end;

  (* Create revoked by server? *)
  if (apCreate in Privileges)
  and not (apCreate in NewPrivileges) then
  begin
    exclude(FData.Privileges, apCreate);
    Owner.PrivilegeRevoked(self, apCreate);
  end;

  (* Install granted by server? *)
  if (apInstall in NewPrivileges)
  and not (apInstall in Privileges) then
  begin
    include(FData.Privileges, apInstall);
    Owner.PrivilegeGranted(self, apInstall);
  end;

  (* Install revoked by server? *)
  if (apInstall in Privileges)
  and not (apInstall in NewPrivileges) then
  begin
    exclude(FData.Privileges, apInstall);
    Owner.PrivilegeRevoked(self, apInstall);
  end;

  (* Appstore granted by server? *)
  if (apAppstore in NewPrivileges)
  and not (apAppstore in Privileges) then
  begin
    include(FData.Privileges, apAppstore);
    Owner.PrivilegeGranted(self, apAppstore);
  end;

  (* Appstore revoked by server? *)
  if (apAppstore in Privileges)
  and not (apAppstore in NewPrivileges) then
  begin
    exclude(FData.Privileges, apAppstore);
    Owner.PrivilegeRevoked(self, apAppstore);
  end;

end;

function TUserSession.ToJSON: string;
begin
  FData.Values := FValues.ToJSonObject();
  result := TJSon.Stringify(FData);
end;

procedure TUserSession.FromJSON(const Data: string);
var
  LValues:  TUserSessionData;
begin
  asm
    @LValues = JSON.parse(@Data);
  end;
  FData := LValues;
  FValues.FromJSon( TJSON.stringify(FData.Values) );
  FData.Values := unassigned;
end;

end.
