unit ragnarok.messages.factory;

interface

uses
  System.Types,
  System.Objects,
  System.Types.Convert,
  System.Time,
  System.JSON,
  System.Dictionaries,

  ragnarok.messages.base,

  System.Streams,
  System.Structure,
  System.Structure.JSON;

type

  // Generic message factory
  TMessageFactory = class(TObject)
  private
    FLUT:       TW3VarDictionary;
  protected
    procedure   RegisterIntrinsic; virtual; abstract;
  public
    procedure   &Register(const MessageClass: TQTXMessageBaseClass);
    function    Build(MessageClassName: string; var Instance: TQTXBaseMessage): boolean;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

//#############################################################################
// TMessageFactory
//#############################################################################

constructor TMessageFactory.Create;
begin
  inherited Create;
  FLUT := TW3VarDictionary.Create;
  RegisterIntrinsic();
end;

destructor TMessageFactory.Destroy;
begin
  FLUT.free;
  inherited;
end;

procedure TMessageFactory.&Register(const MessageClass: TQTXMessageBaseClass);
begin
  if MessageClass <> nil then
  begin
    if not FLUT.Contains(MessageClass.ClassName) then
      FLut.Values[MessageClass.ClassName] := variant(MessageClass);
  end;
end;

function TMessageFactory.Build(MessageClassName: string; var Instance: TQTXBaseMessage): boolean;
begin
  Instance := nil;
  if MessageClassName.Length > 0 then
  begin
    result := FLUT.Contains(MessageClassName);
    if result then
    begin
      var LClass: TQTXMessageBaseClass;
      var LTemp := FLUT.Values[MessageClassName];
      asm
        @LClass = @LTemp;
      end;
      Instance := LClass.Create;
    end;
  end;
end;



end.

