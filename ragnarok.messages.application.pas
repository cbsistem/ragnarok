unit ragnarok.messages.application;

interface

/*
  This unit contains the messages for talking with the Amibian.js desktop
  from a hosted application.

*/

uses
  W3C.WebMessaging,
  System.Types,
  System.Types.Convert,
  System.Objects,
  System.Time,
  System.Dictionaries,
  System.Memory.Buffer,
  System.Streams,
  System.Device.Storage,
  SmartCL.System,
  Ragnarok.json,
  ragnarok.messages.base,
  ragnarok.messages.factory,
  ragnarok.messages.callstack;

const
  STD_GADID_VSCROLL         = '$app:window:vscroll';
  STD_GADID_HSCROLL         = '$app:window:hscroll';

  STD_ATTR_SCROLL_TOTAL     = $BAAD1208;
  STD_ATTR_SCROLL_PAGESIZE  = $BAAD1209;
  STD_ATTR_SCROLL_VALUE     = $BAAD120A;
  STD_ATTR_GADGET_ENABLED   = $BAAD120B;
  STD_ATTR_GADGET_VISIBLE   = $BAAD120C;


type

  // Base-class for response messages from the desktop
  TQTXDesktopMessage = class(TQTXBaseMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Response: string;
    constructor Create(const WithTicket: string); override;
  end;

  // Base-class for request messages from the hosted application
  TQTXApplicationMessage = class(TQTXBaseMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    &MethodName: string;
  end;

  TQTXDesktopError = class(TQTXDesktopMessage)
  end;

  //##########################################################################
  // Directory messages
  //##########################################################################

  // --> Get Directory
  TQTXApplicationGetDirMessage = class(TQTXApplicationMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Path: string;
  end;

  // <-- Directory response
  TQTXDesktopDirResponse = class(TQTXDesktopMessage)
  private
    FDirList:   TNJFileItemList;
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    DirList: TNJFileItemList read FDirList;
    procedure   LoadDirListFromString(const Data: string);
    constructor Create; override;
  end;

  //##########################################################################
  // Window-Title messages
  //##########################################################################

  // --> Set caption
  TQTXApplicationSetWindowTitleMessage = class(TQTXApplicationMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Title: string;
  end;

  //##########################################################################
  // Show File Requester messages
  //##########################################################################

  TQTXApplicationShowFileReqMessage = class(TQTXApplicationMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    InitialPath: string;
    property    FilePattern: string;
  end;

  TQTXApplicationShowFileReqResponse = class(TQTXDesktopMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Filename: string;
  end;

  //##########################################################################
  // Show File Requester messages
  //##########################################################################

  TQTXApplicationSetGadAttrReqMessage = class(TQTXApplicationMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Id:     string;
    property    Attr:   longword;
    property    Value:  variant;
  end;

  TQTXApplicationSetGadAttrReqResponse = class(TQTXDesktopMessage)
  end;

  //--

  // --> Get Attribute
  TQTXApplicationGetGadAttrReqMessage = class(TQTXApplicationMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Id:     string;
    property    Attr:   longword;
  end;

  // <-- Attribute response
  TQTXApplicationGetGadAttrReqResponse = class(TQTXDesktopMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Id:     string;
    property    Attr:   longword;
    property    Value:  variant;
  end;

  //##########################################################################
  // TAmibianMessageFactory
  //##########################################################################

  // Message-factory used by clients
  TAmibianMessageFactory = class(TMessageFactory)
  protected
    procedure RegisterIntrinsic; override;
  end;

  TAPIGetDirListCB = procedure (Success: boolean; TagValue: Variant; Files: TNJFileItemList);
  TAPIGetWindowTitleCB = procedure (Success: boolean; Caption: string);
  TAPISetWindowTitleCB = procedure (Success: boolean);
  TAPIShowRequesterFileCB = procedure (Success: boolean; TagValue: variant; Filename: string);
  TAPIGetAttribCB = procedure (Success: boolean; TagValue: variant; Value: variant);

  IDesktopAPI = interface
    ['{FB31F3A2-89EC-43B9-9B4D-042E3BE61AF5}']
    procedure   GetDirList(Path: string; TagValue: Variant; CB: TAPIGetDirListCB);
    procedure   GetDeviceList(CB: TAPIGetDirListCB);

    procedure   GetWindowTitle(const CB: TAPIGetWindowTitleCB);
    procedure   SetWindowTitle(WindowTitle: string; const CB: TAPISetWindowTitleCB);

    procedure   SetGadgetAttr(TagValue: variant; Id: string; Attr: longword; Value: variant; const CB: TStdCallback);
    procedure   GetGadgetAttr(TagValue: variant; Id: string; Attr: longword; const CB: TAPIGetAttribCB);

    procedure   SetApplicationManifest(const Data: TByteArray; const CB: TStdCallback);

    procedure   ShowRequesterFile(InitialPath: string; FilePattern: string;
                TagValue: variant; CB: TAPIShowRequesterFileCB);
  end;

  TDesktopAPI = class(TW3ErrorObject, IDesktopAPI)
  private
    FMsgPort:     JMessagePort;
    FCallStack:   TWbCallStack;
    FMsgFactory:  TAmibianMessageFactory;

    procedure   HandleHostMessage(event: JMessageEvent);

    procedure   SetApplicationManifest(const Data: TByteArray; const CB: TStdCallback);

    procedure   SetGadgetAttr(TagValue: variant; Id: string; Attr: longword; Value: variant; const CB: TStdCallback);
    procedure   GetGadgetAttr(TagValue: variant; Id: string; Attr: longword; const CB: TAPIGetAttribCB);

    procedure   GetDirList(Path: string; TagValue: variant; CB: TAPIGetDirListCB);
    procedure   GetDeviceList(CB: TAPIGetDirListCB);

    procedure   GetWindowTitle(const CB: TAPIGetWindowTitleCB);
    procedure   SetWindowTitle(WindowTitle: string; const CB: TAPISetWindowTitleCB);

    procedure   ShowRequesterFile(InitialPath: string;
                FilePattern: string; TagValue: variant; CB: TAPIShowRequesterFileCB);

  public
    constructor Create(const Channel: JMessagePort); reintroduce; virtual;
    destructor Destroy; override;
  end;

function  BindToAmibianDesktop(const Channel: JMessagePort): IDesktopAPI;
function  GetDesktopBinding: IDesktopAPI;

implementation

var
  __BindSource: TDesktopAPI;

function  GetDesktopBinding: IDesktopAPI;
begin
  if __BindSource <> nil then
    result := __BindSource as IDesktopAPI
  else
    result := nil;
end;

function BindToAmibianDesktop(const Channel: JMessagePort): IDesktopAPI;
begin
  if __BindSource = nil then
    __BindSource := TDesktopAPI.Create(Channel);
  result := __BindSource as IDesktopAPI;
end;

//#############################################################################
// TQTXDesktopDirResponse
//#############################################################################

constructor TQTXDesktopDirResponse.Create;
begin
  inherited;
  FDirList := new TNJFileItemList();
end;

procedure TQTXDesktopDirResponse.LoadDirListFromString(const Data: string);
begin
  var LTemp := JSON.Parse(Data);
  asm
    (@self.FDirList) = @LTemp;
  end;
end;

procedure TQTXDesktopDirResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
    LParent.WriteOrAdd('filesystem', FDirList)
  else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXDesktopDirResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    if LParent.Exists('filesystem') then
      FDirList := TNJFileItemList( TVariant.AsObject( LParent.Read('filesystem') ) )
    else
      raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["filesystem"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXApplicationShowFileReqMessage
//#############################################################################

procedure TQTXApplicationShowFileReqMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
  begin
    &MethodName := LSource.ReadString('method');

    var LValue := LSource.Locate('parameters', false);
    if LValue <> nil then
    begin
      if LValue.Exists('initialpath') then
        InitialPath := TString.DecodeBase64( LValue.ReadString('initialpath') );

      if LValue.Exists('filepattern') then
        FilePattern := TString.DecodeBase64( LValue.ReadString('filepattern') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["parameters"]);
  end;
end;

procedure TQTXApplicationShowFileReqMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('method', &MethodName);

  var LParams := LTarget.Branch('parameters');
  LTarget.WriteOrAdd('initialpath', TString.EncodeBase64(InitialPath));
  LTarget.WriteOrAdd('filepattern', TString.EncodeBase64(FilePattern));

  inherited WriteObjData(Root);
end;

procedure TQTXApplicationShowFileReqResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    if LParent.Exists('filename') then
      Filename := TString.DecodeBase64( LParent.ReadString('filename') )
    else
      raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["filename"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

procedure TQTXApplicationShowFileReqResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
    LParent.WriteOrAdd('filename', TString.EncodeBase64(Filename))
  else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXApplicationSetGadAttrReqMessage
//#############################################################################

procedure TQTXApplicationSetGadAttrReqMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
  begin
    &MethodName := LSource.ReadString('method');
    var LParams := LSource.Locate('attribute', false);
    if LParams <> nil then
    begin
      id := TString.DecodeBase64( LParams.ReadString('id') );
      attr := LParams.ReadInteger("attr");

      if LParams.Exists("value") then
        //Value := LParams.ReadVariant("value");
        value := LParams.Read("value");
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["attribute"]);
  end;
end;

procedure TQTXApplicationSetGadAttrReqMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('method', &MethodName);

  var LAttrs := LTarget.Branch('attribute');
  LAttrs.WriteOrAdd('id', TString.EncodeBase64(id));
  LAttrs.WriteOrAdd('attr', attr);
  //LAttrs.WriteVariant('value', value);
  LAttrs.WriteOrAdd('value', Value );

  inherited WriteObjData(Root);
end;

//#############################################################################
// TQTXApplicationGetGadAttrReqResponse
//#############################################################################

procedure TQTXApplicationGetGadAttrReqResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
  begin
    var LParams := LSource.Locate('attribute', false);
    if LParams <> nil then
    begin
      id := TString.DecodeBase64( LParams.ReadString('title') );
      attr := LParams.ReadInteger("attr");
      value := LParams.Read("value");
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["parameters"]);
  end;
end;

procedure TQTXApplicationGetGadAttrReqResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  var LParams := LTarget.Branch('attribute');
  LTarget.WriteOrAdd('id', TString.EncodeBase64(id));
  LTarget.WriteOrAdd('Attr', attr);
  LTarget.WriteOrAdd('value', Value );
  inherited WriteObjData(Root);
end;

//#############################################################################
// TQTXApplicationSetGadAttrReqMessage
//#############################################################################

procedure TQTXApplicationGetGadAttrReqMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
  begin
    var LParams := LSource.Locate('attribute', false);
    if LParams <> nil then
    begin
      id := TString.DecodeBase64( LParams.ReadString('title') );
      attr := LParams.ReadInteger("attr");
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["parameters"]);
  end;
end;

procedure TQTXApplicationGetGadAttrReqMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  var LParams := LTarget.Branch('attribute');
  LTarget.WriteOrAdd('id', TString.EncodeBase64(id));
  LTarget.WriteOrAdd('Attr', attr);
  inherited WriteObjData(Root);
end;

//#############################################################################
// TQTXApplicationSetWindowTitleMessage
//#############################################################################

procedure TQTXApplicationSetWindowTitleMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
  begin
    &MethodName := LSource.ReadString('method');

    var LParams := LSource.Locate('parameters', false);
    if LParams <> nil then
    begin
      Title := TString.DecodeBase64( LParams.ReadString('title') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["parameters"]);
  end;
end;

procedure TQTXApplicationSetWindowTitleMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('method', &MethodName);

  var LParams := LTarget.Branch('parameters');
  LParams.WriteOrAdd('title', TString.EncodeBase64(Title));

  inherited WriteObjData(Root);
end;

//#############################################################################
// TQTXApplicationMessage
//#############################################################################

procedure TQTXApplicationGetDirMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
  begin
    &MethodName := LSource.ReadString('method');

    var LValue := LSource.Locate('parameters', false);
    if LValue <> nil then
    begin
      Path := TString.DecodeBase64( LValue.ReadString('path') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["parameters"]);
  end;
end;

procedure TQTXApplicationGetDirMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('method', &MethodName);

  var LParams := LTarget.Branch('parameters');
  LTarget.WriteOrAdd('path', TString.EncodeBase64(Path));

  inherited WriteObjData(Root);
end;

//#############################################################################
// TQTXApplicationMessage
//#############################################################################

procedure  TQTXApplicationMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
    &MethodName := LSource.ReadString('method');
end;

procedure  TQTXApplicationMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('method', &MethodName);
  inherited WriteObjData(Root);
end;

//#############################################################################
// TQTXDesktopMessage
//#############################################################################

constructor TQTXDesktopMessage.Create(const WithTicket: string);
begin
  inherited Create(WithTicket);
  Response := 'OK';
end;

procedure  TQTXDesktopMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LSource := Root.Locate(ClassName, false);
  if LSource <> nil then
    Response := LSource.ReadString('response');
end;

procedure  TQTXDesktopMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('response', Response);
  inherited WriteObjData(Root);
end;

//#############################################################################
// TDesktopAPI
//#############################################################################

constructor TDesktopAPI.Create(const Channel: JMessagePort);
begin
  inherited Create;
  FCallStack := TWbCallStack.Create();
  FMsgFactory := TAmibianMessageFactory.Create();

  FMsgPort := Channel;
  FMsgport.onmessage := @HandleHostMessage;
end;

destructor TDesktopAPI.Destroy;
begin
  FMsgPort.onmessage := nil;
  FMsgPort := nil;
  FCallstack.free;
  FMsgFactory.free;
  inherited;
end;

procedure TDesktopAPI.HandleHostMessage(event: JMessageEvent);
var
  LId:  string;
  LEnvelope: TQTXBaseMessage;
begin
  // Do a quick parse and extract identifier
  var obj := JSON.parse(string(event.data));
  if (obj["identifier"]) then
  begin
    LId := obj["identifier"];
    obj := nil;
  end else
    raise EW3Exception.Create('Unknown or damaged message packet error');

  // Lookup identifier with factory and get instance
  // If successful, load JSON into correct envelope
  if not FMsgFactory.Build(LId, LEnvelope) then
    raise EW3Exception.CreateFmt('Unknown or unsupported message [%s] error', [LId])
  else
    LEnvelope.Parse(string(event.data));

  try
    if FCallStack.Contains(LEnvelope.Ticket) then
      FCallstack.Invoke(LEnvelope.Ticket, LEnvelope)
    else
      raise EW3Exception.CreateFmt
        ('Unknown message [%s], could not map response', [LEnvelope.ClassName()]);
  finally
    LEnvelope.free;
  end;
end;

procedure TDesktopAPI.GetDeviceList(CB: TAPIGetDirListCB);
begin
end;

procedure TDesktopAPI.ShowRequesterFile(InitialPath: string;
            FilePattern: string; TagValue: variant; CB: TAPIShowRequesterFileCB);
begin
  ClearLastError();

  var LMessage := TQTXApplicationShowFileReqMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'ShowFileOpenRequester';
    LMessage.InitialPath := InitialPath;
    LMessage.FilePattern := FilePattern;

    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false, TagValue, '');
          exit;
        end;

        if assigned(CB) then
          CB(true, TagValue, TQTXApplicationShowFileReqResponse(response).Filename);
      end);

    FMsgPort.postMessage( LMessage.Serialize() );
  finally
    LMessage.free;
  end;
end;

procedure TDesktopAPI.GetDirList(Path: string; TagValue: variant; CB: TAPIGetDirListCB);
begin
  ClearLastError();
  var LMessage := TQTXApplicationGetDirMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'GetDirList';
    LMessage.Path := Path;

    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false, TagValue, nil);
          exit;
        end;

        if assigned(CB) then
          CB(true, TagValue, TQTXDesktopDirResponse(Response).DirList);
      end);

    FMsgPort.postMessage( LMessage.Serialize() );
  finally
    LMessage.free;
  end;
end;

procedure TDesktopAPI.GetWindowTitle(const CB: TAPIGetWindowTitleCB);
begin
  if ErrorOptions.AutoResetError then
    ClearLastError();

  var LMessage := TQTXApplicationMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'GetWindowTitle';
    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false, '');
          exit;
        end;

        if assigned(CB) then
          CB(true, TQTXDesktopMessage(response).Response);
      end);

    FMsgPort.postMessage( LMessage.Serialize() );
  finally
    LMessage.free;
  end;
end;

procedure TDesktopAPI.SetWindowTitle(WindowTitle: string; const CB: TAPISetWindowTitleCB);
begin
  if ErrorOptions.AutoResetError then
    ClearLastError();

  var LMessage := TQTXApplicationSetWindowTitleMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'SetWindowTitle';
    LMessage.title := WindowTitle;

    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false);
          exit;
        end;

        if assigned(CB) then
          CB(true);
      end);

    FMsgPort.postMessage( LMessage.Serialize() );
  finally
    LMessage.free;
  end;
end;

//#############################################################################
// TAmibianMessageFactory
//#############################################################################

procedure TDesktopAPI.SetGadgetAttr(TagValue: variant; Id: string; Attr: longword; Value: variant; const CB: TStdCallback);
begin
  var LMessage := TQTXApplicationSetGadAttrReqMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'SetGadgetAttr';
    LMessage.Id := id;
    LMessage.Attr := Attr;
    LMessage.Value := Value;

    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false);
          exit;
        end;

        if assigned(CB) then
          CB(true);
      end);

    FMsgPort.postMessage( LMessage.Serialize() );
  finally
    LMessage.free;
  end;
end;

procedure TDesktopAPI.GetGadgetAttr(TagValue: variant; Id: string; Attr: longword; const CB: TAPIGetAttribCB);
begin
  var LMessage := TQTXApplicationGetGadAttrReqMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'GetGadgetAttr';
    LMessage.id := Id;
    LMessage.Attr := Attr;

    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        // Check for error
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false, TagValue, null);
          exit;
        end;


        if assigned(CB) then
          CB(true, TagValue, TQTXApplicationGetGadAttrReqResponse(Response).Value );
      end);

    FMsgPort.postMessage( LMessage.Serialize() );
  finally
    LMessage.free;
  end;
end;

procedure TDesktopAPI.SetApplicationManifest(const Data: TByteArray; const CB: TStdCallback);
begin
  var LMessage := TQTXApplicationMessage.Create( TQTXBaseMessage.CreateTicket() );
  try
    LMessage.&MethodName := 'SetApplicationManifest';

    if Data.length > 0 then
      LMessage.Attachment.AppendBytes(Data);

    FCallstack.Commit(LMessage.Ticket,
      procedure (const Response: TQTXBaseMessage)
      begin
        if Response is TQTXDesktopError then
        begin
          SetLastError( TQTXDesktopError(response).Response);
          if assigned(CB) then
            CB(false);
          exit;
        end;

        if assigned(CB) then
          CB(true);
      end);

    FMsgPort.postMessage( LMessage.Serialize() );


  finally
    LMessage.free;
  end;
end;

//#############################################################################
// TAmibianMessageFactory
//#############################################################################

procedure TAmibianMessageFactory.RegisterIntrinsic;
begin
  &Register(TQTXDesktopMessage);
  &Register(TQTXDesktopDirResponse);
  &Register(TQTXApplicationShowFileReqResponse);
  &Register(TQTXDesktopError);
  &Register(TQTXResponseMessage);
end;


end.
