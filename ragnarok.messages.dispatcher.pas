unit ragnarok.messages.dispatcher;

interface

uses
  System.Types,
  System.Objects,
  System.Types.Convert,
  System.Time,
  System.JSON,
  System.Dictionaries,

  ragnarok.messages.base,
  ragnarok.messages.network,
  ragnarok.sessions,

  SmartNJ.Server.WebSocket,

  System.Streams,
  System.Structure,
  System.Structure.JSON;

type

  // Exceptions
  ERagnarokMessageDispatcher  = class(EW3Exception);

  // Forward declarations
  TRagnarokMessageInfo  = class;


  TRRMessageHandler = procedure (Sender: TObject; Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXClientMessage);

  TRagnarokMessageInfo = class
    MessageClass:   TQTXMessageBaseClass;
    MessageHandler: TRRMessageHandler;
  end;

  TRagnarokMessageDispatcher = class(TObject)
  private
    FItems:     array of TRagnarokMessageInfo;
    FNameLUT:   TW3ObjDictionary;
    //FIDLUT:     TW3ObjDictionary;
  public
    property    Items[const index: integer]: TRagnarokMessageInfo read ( FItems[index] ); default;
    property    Count: integer read (FItems.Count);

    function    GetMessageInfoForName(MessageClassName: string): TRagnarokMessageInfo;
    function    GetMessageInfoForClass(const MessageClass: TQTXMessageBaseClass): TRagnarokMessageInfo;
    procedure   RegisterMessage(const MessageClass: TQTXMessageBaseClass; const Handler: TRRMessageHandler);

    procedure   Clear; virtual;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;


implementation


resourcestring
  CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_CLASSNIL =
  'Failed to register message, classtype was nil error';

  CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLERNIL =
  'Failed to register message, handler as nil error';

  CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLEREXISTS =
  'Failed to register message, a handler for that message already exists error';

//#############################################################################
// TRagnarokMessageDispatcher
//#############################################################################

constructor TRagnarokMessageDispatcher.Create;
begin
  inherited Create;
  FNameLUT := TW3ObjDictionary.Create;
  //FIDLUT := TW3ObjDictionary.Create;
end;

destructor TRagnarokMessageDispatcher.Destroy;
begin
  if FItems.Count > 0 then
    Clear();
  FNameLut.free;
  //FIdLUT.free;
  inherited;
end;

procedure TRagnarokMessageDispatcher.Clear;
begin
  try
    for var info in FItems do
    begin
      info.free;
    end;
  finally
    FItems.clear();
    FNameLUT.clear();
    //FIdLUT.clear();
  end;
end;

function TRagnarokMessageDispatcher.GetMessageInfoForClass
  (const MessageClass: TQTXMessageBaseClass): TRagnarokMessageInfo;
begin
  if MessageClass <> nil then
  begin
    for var Info in FItems do
    begin
      if Info.MessageClass = MessageClass then
      begin
        result := info;
        break;
      end;
    end;
  end;
end;

function TRagnarokMessageDispatcher.GetMessageInfoForName
  (MessageClassName: string): TRagnarokMessageInfo;
begin
  MessageClassName := MessageClassName.Trim().ToLower();
  if MessageClassName.Length > 0 then
  begin
    if FNameLUT.Contains(MessageClassName) then
      result := TRagnarokMessageInfo(FNameLUT.Values[MessageClassName]);
  end;
end;

procedure TRagnarokMessageDispatcher.RegisterMessage
  (const MessageClass: TQTXMessageBaseClass; const Handler: TRRMessageHandler);
begin
  if MessageClass <> nil then
  begin
    if assigned(Handler) then
    begin
      var LName := MessageClass.ClassName.Trim().ToLower();
      if not FNameLUT.Contains(LName) then
      begin
        // Create message handler info
        var Info := TRagnarokMessageInfo.Create;
        Info.MessageClass := MessageClass;
        Info.MessageHandler := @Handler;

        // Add to linear-list
        FItems.Add(Info);

        // Register by name in our string->object lookup table
        FNameLUT.Values[LName] := Info;

      end else
      raise ERagnarokMessageDispatcher.Create(CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLEREXISTS);
    end else
    raise ERagnarokMessageDispatcher.Create(CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_HANDLERNIL);
  end else
  raise ERagnarokMessageDispatcher.Create(CNT_ERR_MESSAGEHANDLERS_REGISTER_FAILED_CLASSNIL);
end;


end.
