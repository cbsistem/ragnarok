//#############################################################################
// ______                                  _
// | ___ \ Smart Mobile Studio            | |
// | |_/ /__ _  __ _ _ __   __ _ _ __ ___ | | __
// |    // _` |/ _` | '_ \ / _` | '__/ _ \| |/ /
// | |\ \ (_| | (_| | | | | (_| | | | (_) |   <
// \_| \_\__,_|\__, |_| |_|\__,_|_|  \___/|_|\_\
//              __/ |
//             |___/          Powered by Node.js
//
//#############################################################################

unit Ragnarok.Server;

interface

{.$DEFINE DEBUG}

uses
  System.Types,
  System.Time,
  System.Objects,
  System.Types.Convert,
  System.JSON,
  System.Dictionaries,
  System.Streams,
  System.Reader,
  System.Writer,
  System.DateUtils,

  Ragnarok.Types,
  ragnarok.messages.base,
  ragnarok.messages.network,
  ragnarok.messages.factory,
  ragnarok.messages.dispatcher,
  Ragnarok.Sessions,
  Ragnarok.Cmd.Parser,

  System.Device.Storage,
  SmartNJ.Device.Storage,

  NodeJS.Core,
  NodeJS.child_process,
  NodeJS.fs,
  NodeJS.path,

  SmartNJ.Application,
  SmartNJ.System,
  SmartNJ.Streams,
  SmartNJ.Server,
  SmartNJ.Network,
  SmartNJ.Server.Http,
  SmartNJ.Server.Https,
  SmartNJ.Server.WebSocket,
  SmartNJ.Server.WebSocketSecure;

type

  // Forward declarations
  TCustomRagnarokServer = partial class;
  TRagnarokServer       = class;

  // Exception classes
  ERagnarokServer           = class(EW3Exception);

  // Classbacks and events
  TRRServerConnectedEvent = procedure (Sender: TObject; Socket: TNJWebSocketSocket);
  TRRServerDisconnectedEvent = procedure (Sender: TObject; Socket: TNJWebSocketSocket);
  TRRServerMessageEvent = procedure (Sender: TObject; Message: TQTXBaseMessage; Session: TUserSession; var Reject: boolean; var Reason: string);
  TRRServerAuthenticateEvent = procedure (Sender: TObject; Message: TQTXLoginRequest;
    Session: TUserSession; var Allow: boolean);

  TRRServerGetDevice = procedure (Sender: TObject; var Device: TW3NodeStorageDevice);
  TRRServerLogEvent = procedure (Sender: TObject; LogText: string);

  TRRServerInitUserSession = procedure (Sender: TObject; Session: TUserSession);

  TRagnarokMessageFactory = class(TMessageFactory)
  protected
    procedure   RegisterIntrinsic; override;
  end;

  TCustomRagnarokServer = partial class(TRagnarokServerBase)
  private
    FSecure:    boolean;
    FServer:    TNJWebSocketHybridServer;
    FMessages:  TRagnarokMessageDispatcher;
    FFactory:   TRagnarokMessageFactory;

    // Our event handlers
    procedure   HandleServerStarted(Sender: TObject);
    procedure   HandleServerStopped(Sender: TObject);
    procedure   HandleConnected(Sender: TObject; Socket: TNJWebSocketSocket);
    procedure   HandleDisconnected(Sender: TObject; Socket: TNJWebSocketSocket);
    procedure   HandleTextMessage(Sender: TObject; Socket: TNJWebSocketSocket; Info: TNJWebsocketMessage);

  protected
    procedure   SendRejection(const Socket: TNJWebSocketSocket;
                Ticket: string; const Cause: string; const Close: boolean);

    procedure   SendUnSupported(const Socket: TNJWebSocketSocket; Ticket: string);

    procedure   SendInternalError(const Socket: TNJWebSocketSocket;
                Ticket: string; const Cause: string; const Close: boolean);

    procedure   Dispatch(const Socket: TNJWebSocketSocket; const Envelope: TQTXBaseMessage);
  public
    property    Secure: boolean read FSecure;
    property    Server: TNJWebSocketHybridServer read FServer;

    property    MessageDispatch: TRagnarokMessageDispatcher read FMessages;
    property    MessageFactory: TRagnarokMessageFactory read FFactory;

    procedure   WriteToLog(const Text: string);
    procedure   WriteToLogF(const Text: string; const Values: array of const);

    constructor Create(const Secure: boolean); virtual;
    destructor  Destroy; override;
  published
    property    OnServerStarted: TNotifyEvent;
    property    OnServerStopped: TNotifyEvent;
    property    OnClientConnected: TRRServerConnectedEvent;
    property    OnClientDisconnected: TRRServerDisconnectedEvent;
    property    OnClientMessage: TRRServerMessageEvent;
    property    OnLogMessage: TRRServerLogEvent;
  end;

  TRagnarokServerExecuteCB = procedure (Success: boolean; TagValue: variant; Data: string);

  TRagnarokServer = class(TCustomRagnarokServer)
  private
    // This is the handler for the login authentication
    procedure   DoAuthenticate(Sender: TObject;
                  Socket: TNJWebSocketSocket;
                  Session: TUserSession;
                  Request: TQTXClientMessage);

    // This is the handler for the FileIO message
    // It will parse the command and execute one of the handlers
    // below (such as DIR, Read etc. etc)
    procedure   DoFileIO(Sender: TObject;
                  Socket: TNJWebSocketSocket;
                  Session: TUserSession;
                  Request: TQTXClientMessage);

    procedure   DoCmdDir(FileSystem: TW3NodeStorageDevice;
                  Command: TWbCMDInfo; Socket: TNJWebSocketSocket;
                  Session: TUserSession; Request: TQTXFileIORequest);

    procedure   DoCmdRead(FileSystem: TW3NodeStorageDevice;
                  Command: TWbCMDInfo; Socket: TNJWebSocketSocket;
                  Session: TUserSession; Request: TQTXFileIORequest);

    procedure   DoCmdRun(FileSystem: TW3NodeStorageDevice;
                  Command: TWbCMDInfo; Socket: TNJWebSocketSocket;
                  Session: TUserSession; Request: TQTXFileIORequest);

  protected
    // This is needed by the FileIO messages. The server calls out with an
    // event and expects to get a mounted, working instance back.
    // Note: please remember that this only happens once or twice! It is not
    // invoked per session or user. If a user has a dedicated folder on the
    // server, make sure you calculate the path to the user's root,
    // and just keep it in the session-object
    function    GetFileSystem: TW3NodeStorageDevice; virtual;

    procedure   ExecuteExternalJS(Params: array of string;
      TagValue: variant; const CB: TRagnarokServerExecuteCB);

    // We need to override this to make sure we register
    // our supported message classes, otherwise the server wont know
    // what messages to dispatch and where
    procedure   InitializeMessages; override;

  published
    property    OnAuthenticate: TRRServerAuthenticateEvent;
    property    OnInitAfterLogin: TRRServerInitUserSession;
    property    OnGetStorageDevice: TRRServerGetDevice;
  end;

implementation

//#############################################################################
// TRagnarokMessageFactory
//#############################################################################

procedure TRagnarokMessageFactory.RegisterIntrinsic;
begin
  // Register all supported message classes
  &Register(TQTXLoginRequest);
  &Register(TQTXFileIORequest);
end;

//#############################################################################
// TCustomRagnarokServer
//#############################################################################

constructor TCustomRagnarokServer.Create(const Secure: boolean);
begin
  inherited Create;

  FSecure := Secure;

  // Setup the ragnarok message factory
  FFactory := TRagnarokMessageFactory.Create;

  // Initialize the message-handler repository.
  // You register message-types with a callback, and when such a message
  // is dispatched, its automatically delivered to the callback.
  // This is needed because Ragnarok has no "fixed" amount of messages,
  // it can support 2 to 2000 message types, depending on whats registered.
  FMessages := TRagnarokMessageDispatcher.Create;

  // Create our server, but make sure we catch any
  // exceptions. If a package or dependency is missing an exeption
  // will be invoked here. In that case we dump the message and terminate
  try
    case FSecure of
    true:   FServer := TNJWebSocketServerSecure.Create;
    false:  FServer := TNJWebSocketServer.Create;
    end;
  except
    on e: exception do
    begin
      writelnF("Failed to create websocket server: %s", [e.message]);
      writeln("Please make sure module ws is installed via npm");
      application.Terminate();
      exit;
    end;
  end;

  // We want the server to keep track of our clients
  // in case we want to enumerate everyone connected or send global
  // messages to everyone.
  FServer.ClientTracking := true;

  // Just pick a port. Websocket uses http as a foundation so you can
  // bind to port 80 and do both normal http and upgraded communication
  // from within the same server
  FServer.Port := 1881;

  // Attach some event handlers
  FServer.OnAfterServerStarted := @HandleServerStarted;
  FServer.OnAfterServerStopped := @HandleServerStopped;
  FServer.OnClientConnected :=  @HandleConnected;
  FServer.OnClientDisconnected := @HandleDisconnected;
  FServer.OnTextMessage := @HandleTextMessage;

  // Allow partial-classes to register their modules
  InitializeMessages();
end;

destructor TCustomRagnarokServer.Destroy;
begin
  try
    if FServer <> nil then
    begin
      try
        if FServer.Active then
          FServer.Stop();
      finally
        FServer.free;
        FServer := nil;
      end;
    end;
  finally
    FMessages.free;
    FFactory.free;
  end;
  inherited;
end;

procedure TCustomRagnarokServer.WriteToLog(const Text: string);
begin
  if assigned(OnLogMessage) then
    OnLogMessage(self, Text);
end;

procedure TCustomRagnarokServer.WriteToLogF(const Text: string; const Values: array of const);
begin
  if assigned(OnLogMessage) then
    WriteToLog(Format(Text, Values));
end;

procedure TCustomRagnarokServer.HandleConnected(Sender: TObject; Socket: TNJWebSocketSocket);
begin
  // Create a session object
  var LSession := SessionManager.Add(Socket.RemoteAddress);
  if LSession <> nil then
  begin
    Socket.TagData := LSession.Identifier;
    WriteToLogF("Session %s is delegated for socket [%s] @ %s", [LSession.Identifier, Socket.Name, Socket.RemoteAddress]);
  end else
  raise ERagnarokServer.CreateFmt("Failed to create session: %s", [SessionManager.LastError]);

  if assigned(OnClientConnected) then
    OnClientConnected(self, Socket);
end;

procedure TCustomRagnarokServer.HandleDisconnected(Sender: TObject; Socket: TNJWebSocketSocket);
begin
  if assigned(OnClientDisconnected) then
    OnClientDisconnected(self, Socket);

  // Count number of connections from the same source
  WriteToLogF("Counting connections from same host: %d", [FServer.Count]);
  var LConnectionsFromSame := 0;
  for var x := 0 to FServer.Count -1 do
  begin
    var Item := FServer.Clients[x];
    if Item.RemoteAddress.toLower() = Socket.RemoteAddress.toLower() then
      inc(LConnectionsFromSame);
  end;
  WriteToLogF("Counted %d for host %s", [LConnectionsFromSame, Socket.RemoteAddress]);

  // Get the session object for this connection
  // Note: if it's not attainable it has already been released
  var LSession := SessionManager.GetSessionbyId(Socket.TagData);
  if LSession <> nil then
  begin
    WriteToLogF("Session %s for socket %s has disconnected", [LSession.Identifier, Socket.Name]);
    if LConnectionsFromSame < 1 then
    begin
      WriteToLog("Last connection for host, killing session object");
      LSession.free;
    end;
  end;
end;

procedure TCustomRagnarokServer.SendInternalError(const Socket: TNJWebSocketSocket;
  Ticket: string; const Cause: string; const Close: boolean);
begin
  if Socket <> nil then
  begin
    // Internal error
    var reply := TQTXServerError.Create(Ticket);
    try
      reply.Code := CNT_MESSAGE_CODE_ERROR;
      reply.Response := format(CNT_MESSAGE_TEXT_ERROR, [Cause]);

      if Socket.ReadyState = rsOpen then
      begin
        Socket.Send(reply.Serialize());
        if Close then
          Socket.Close();
      end;
    finally
      reply.free;
    end;
  end;
end;

procedure TCustomRagnarokServer.SendRejection(const Socket: TNJWebSocketSocket;
  Ticket: string; const Cause: string; const Close: boolean);
begin
  if Socket <> nil then
  begin
    // Unknown or unsupported identifier, return error
    var reply := TQTXServerError.Create(Ticket);
    try
      reply.Code := CNT_MESSAGE_CODE_REJECTED;
      reply.Response := Format(CNT_MESSAGE_TEXT_REJECTED, [Cause]);

      if Socket.ReadyState = rsOpen then
      begin
        Socket.Send(Reply.Serialize());
        if Close then
          Socket.Close();
      end;
    finally
      reply.free;
    end;
  end;
end;

procedure TCustomRagnarokServer.SendUnSupported(const Socket: TNJWebSocketSocket; Ticket: string);
begin
  if Socket <> nil then
  begin
    // Unknown or unsupported identifier, return error
    var reply := TQTXServerError.Create(Ticket);
    try
      reply.Code := CNT_MESSAGE_CODE_ERROR;
      reply.Response := CNT_MESSAGE_TEXT_ERROR;

      if Socket.ReadyState = rsOpen then
        Socket.Send( Reply.Serialize() );
    finally
      reply.free;
    end;
  end;
end;

procedure TCustomRagnarokServer.HandleTextMessage(Sender: TObject;
  Socket: TNJWebSocketSocket; Info: TNJWebsocketMessage);
var
  LIdentifier: string;
  MsgEnvelope: TQTXBaseMessage;
begin
  // Do a quick parse of the message
  var MsgObject := JSON.parse(info.wiText);

  // Extract envelope identifier, always check a pointer before use
  if (MsgObject["identifier"]) then
    LIdentifier := MsgObject["identifier"];

  // Check that what we got has length
  if LIdentifier.Length >=1 then
  begin
    // Lookup the identifier and create the message-envelope
    if FFactory.Build(LIdentifier, MsgEnvelope) then
    begin
      {$IFDEF DEBUG}
      writeln("Factory created envelope of type " + MsgEnvelope.ClassName);
      {$ENDIF}

      // Initialize envelope from JSON
      try
        MsgEnvelope.Parse(info.wiText);
      except
        on e: exception do
        begin
          writeln("Parsing failed with:" + e.message);
        end;
      end;

      // Fire event to check if this should be rejected
      if assigned(OnClientMessage) then
      begin
        // init rejection
        var LReject := false;
        var LReason := '';
        var LSession: TUserSession := SessionManager.GetSessionbyId(Socket.TagData);
        OnClientMessage(Self, MsgEnvelope, LSession, LReject, LReason);

        // Should we reject this message?
        if LReject then
        begin
          // Send rejection message and close
          SendRejection(Socket, MsgEnvelope.ticket, LReason, true);
          exit;
        end;
      end;

      // Dispatch message
      Dispatch(Socket, MsgEnvelope);
    end else
    SendUnSupported(Socket, '');
  end else
  SendUnSupported(Socket, '');
end;

procedure TCustomRagnarokServer.Dispatch(const Socket: TNJWebSocketSocket; const Envelope: TQTXBaseMessage);
var
  LInfo: TRagnarokMessageInfo;
  LSession: TUserSession;
begin

  {$IFDEF DEBUG}
  writeln("Entering dispatch for socket " + Socket.TagData);
  {$ENDIF}

  if Envelope <> nil then
    WriteToLogF('Recieved message @ %s of type %s', [TimeToStr(now), Envelope.ClassName])
  else
  begin
    WriteToLogF('Recieved unknown message @ %s', [TimeToStr(now)]);
    exit;
  end;

  // look-up the message identifier to see if its supported
  try
    LInfo := FMessages.GetMessageInfoForName(Envelope.ClassName);
  except
    on e: exception do
    begin
      WriteToLog('Failed to locate message-handler info:' + e.message);
      SendInternalError(Socket, Envelope.Ticket,
        'Failed to locate message-handler info:' + e.message, false);
    end;
  end;

  if LInfo = nil then
  begin
    WriteToLog('Failed to find messagehandler for:' + Envelope.ClassName);
    SendInternalError(Socket, Envelope.Ticket,
      'No handler is registered for this message-type', false);
    exit;
  end;
  WriteToLog('Handler-Info found');

  if not assigned(LInfo.MessageHandler) then
  begin
    WriteToLog('Invalid messagehandler for:' + Envelope.ClassName);
    SendInternalError(Socket, Envelope.Ticket,
      'Invalid messagehandler for', false);
    exit;
  end;
  WriteToLog('Handler-Info is valid');

  // locate session object
  try
    LSession := SessionManager.GetSessionbyId(Socket.TagData);
  except
    on e: exception do
    begin
      WriteToLog('Failed to locate session object:' + e.message);
      SendInternalError(Socket, Envelope.Ticket, e.message, false);
      exit;
    end;
  end;

  if LSession = nil then
  begin
    WriteToLog('Session could not be found:' + SessionManager.LastError);
    exit;
  end;

  // Execute handler-code
  try
    LInfo.MessageHandler(self, Socket, LSession, TQTXClientMessage(Envelope))
  except
    on e: exception do
    begin
      // Write error to log
      WriteToLog('Failed to execute messagehandler:' + e.message);

      // Mirror the exception back to the client
      SendRejection(Socket, Envelope.Ticket, e.message, false);
      exit;
    end;
  end;

  WriteToLog('Message-Handler Executed');
end;

procedure TCustomRagnarokServer.HandleServerStarted(Sender: TObject);
begin
  if assigned(OnServerStarted) then
    OnServerStarted(self);
  WriteToLogF("Server started, listening on port %d", [FServer.port]);
end;

procedure TCustomRagnarokServer.HandleServerStopped(Sender: TObject);
begin
  if assigned(OnServerStopped) then
    OnServerStopped(Self);
  WriteToLog("Server stopped.");
end;

//#############################################################################
// TRagnarokServer
//#############################################################################

procedure TRagnarokServer.InitializeMessages;
begin
  inherited;
  MessageDispatch.RegisterMessage(TQTXLoginRequest, @DoAuthenticate);
  MessageDispatch.RegisterMessage(TQTXFileIORequest, @DoFileIO);
end;

function TRagnarokServer.GetFileSystem: TW3NodeStorageDevice;
begin
  var LDevice: TW3NodeStorageDevice;
  if assigned(OnGetStorageDevice) then
  begin
    OnGetStorageDevice(self, LDevice);
    result := LDevice;
    exit;
  end;
end;


procedure TRagnarokServer.DoAuthenticate(Sender: TObject; Socket: TNJWebSocketSocket;
          Session: TUserSession; Request: TQTXClientMessage);
begin
  var LValid := false;
  var LReq := TQTXLoginRequest(Request);

  // Set the username early, since we dont really know if this
  // is a legal login just yet
  Session.username := LReq.Username;

  // Event available to handle authentication?
  if assigned(OnAuthenticate) then
  begin
    // Bombs away bitches!
    var Allow: boolean := true;
    OnAuthenticate(self, LReq, Session, Allow);
    LValid := Allow = true;
  end else
  begin
    SendInternalError(Socket, Request.Ticket, 'Server authentication not available', true);
    exit;
  end;

  try
    // Did we authenticate?
    if LValid then
    begin
      // I want your drives, both of them!
      var LFileSystem := GetFileSystem();

      // Set some initial values
      // Note: This **SHOULD** be changed in the InitSession event !
      // You really dont want users running around packing full admin
      // rights in your bloody sandbox
      Session.LoginTime := now;
      Session.State := asActive;
      Session.Privileges := [apRead, apWrite, apCreate, apExecute, apInstall, apAppstore];

      // Let it rip bitches!
      if assigned(OnInitAfterLogin) then
        OnInitAfterLogin(self, Session);
    end;
  finally

    // If we authenticated properly, it's all good baby, pop your clothes
    // on the stool, grab a pube and snuggle up
    if LValid then
    begin
      var Response := TQTXLoginResponse.Create(Request.Ticket);
      try
        Response.Username := LReq.Username;
        Response.SessionID := Socket.TagData;
        Response.Code := CNT_MESSAGE_CODE_OK;
        Response.Response := CNT_MESSAGE_TEXT_OK;
        Socket.Send( Response.Serialize() );
      finally
        Response.free;
        Response := nil;
      end;
    end else
    begin
      // This will only execute if something went
      // terribly wrong and thus authentication failed
      var reply := TQTXServerError.Create(Request.Ticket);
      reply.Code := CNT_MESSAGE_CODE_ERROR;
      reply.Response := 'Login failed, invalid username or password';
      Socket.Send(reply.Serialize());
    end;
  end;
end;

procedure TRagnarokServer.DoCmdRun(FileSystem: TW3NodeStorageDevice;
          Command: TWbCMDInfo; Socket: TNJWebSocketSocket;
          Session: TUserSession; Request: TQTXFileIORequest);
begin
  var xsend := Command.ciCommand + " " + Command.ciParams.join(",");
  child_process.exec(xsend, procedure (error: JError; stdout: JNodeBuffer; stderr: JNodeBuffer)
  begin
    // Did we fail?
    if assigned(error) then
    begin
      var ErrText := format("Failed to execute [%s], system failed with: %s", [xSend, error.Message]);
      WriteToLog(ErrText);

      WriteToLog("Sending internal error to client");
      SendInternalError(Socket, Request.Ticket, ErrText, false);
      exit;
    end;

    // Get text buffered in StdOut
    var LText := stdout.toString("utf8");

    // This examines your user folder and sends back the files [dirlist]
    var Response := TQTXFileIOResponse.Create(Request.Ticket);
    try
      Response.Attachment.Release();
      Response.Attachment.AppendBytes(Response.Attachment.StringToBytes(LText));

      Response.Code := CNT_MESSAGE_CODE_OK;
      Response.Response := CNT_MESSAGE_TEXT_OK;
      Socket.Send(Response.Serialize());
    finally
      WriteToLog(Response.Serialize());
      Response.free;
    end;
  end);
end;


//Note: This implementation must be changed to support
// partial reads. At the moment it has only one parameter, the filename,
// but it should also support 3:
//    filename
//    start-offset [numeric]
//    read-size [numeric]

procedure TRagnarokServer.DoCmdRead(FileSystem: TW3NodeStorageDevice;
          Command: TWbCMDInfo; Socket: TNJWebSocketSocket;
          Session: TUserSession; Request: TQTXFileIORequest);
begin

  if Command.ciParams.length < 1 then
  begin
    SendRejection(Socket, Request.Ticket, "No filename error", false);
    exit;
  end;

  // Get the local filesystem
  var Filesys := GetFileSystem();

  // Check that its available
  if FileSys = nil then
  begin
    // Not available? OK, send an unsupported message
    SendUnSupported(Socket, Request.Ticket);
    exit;
  end;

  // Make sure its active
  if not FileSys.Active then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: filesystem not mounted error", false);
    exit;
  end;

  // Make sure user has logged in
  if Session.State <> asActive then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: user not authenticated error", false);
    exit;
  end;

  // Make sure user has read access
  if not (apRead in Session.Privileges) then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: user lacks read privilege error", false);
    exit;
  end;

  var LPath := IncludeTrailingPathDelimiter(Session.RootFolder);

  // All paths are relative to the userpath, so we brutally append
  // Get the target path
  var temp :=  Command.ciParams.Join('');
  var LInitialPath := temp;

  // prefixed with posix home moniker? Just remove it
  if temp.startswith('~/') then
    delete(Temp, 1, 2);

  // Build complete target path
  LPath := IncludeTrailingPathDelimiter(LPath) + temp;

  LPath := NodePathAPI.Normalize(LPath);

  var LServicePath := IncludeTrailingPathDelimiter('.\services');
      LServicePath := IncludeTrailingPathDelimiter(LServicePath) + 'messageConstructor.js';

  writeln("Loading file:" + LPath);

  ExecuteExternalJS([LServicePath, request.ticket, LPath], LInitialPath,
  procedure (Success: boolean; TagValue: variant; Data: string)
  begin
    case Success of
    true:
      begin
        writelnF("Reading file-data was successfull [%s], %s", [TagValue, Request.Ticket]);
        //writeln(data);
        Socket.Send(Data);
        exit;
      end;
    false:
      begin
        writelnF("Reading file-data failed [%s], %s", [TagValue, Request.Ticket]);
        SendInternalError(Socket, Request.Ticket,
          Format("Failed to read file, %s, %s", [Data, Request.Ticket]), false);
      end;
    end;
  end);

end;

procedure TRagnarokServer.ExecuteExternalJS(Params: array of string;
  TagValue: variant; const CB: TRagnarokServerExecuteCB);
begin
  var LTask: JChildProcess;

  Params.insert(0, '--no-warnings');

  // Spawn a new process, this creates a new shell interface
  try
    LTask := child_process().spawn('node', Params );
  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(false, TagValue, e.message);
      exit;
    end;
  end;

  // Map general errors on process level
  LTask.on('error', procedure (error: variant)
  begin
    writeln("error->" + error.toString());
    WriteToLog(error.toString());
  end);

  // map stdout so we capture the output
  LTask.stdout.on('data', procedure (data: variant)
  begin
    if assigned(CB) then
      CB(true, TagValue, data.toString());
  end);

  // map stderr so we can capture exception messages
  LTask.stderr.on('data', procedure (error:variant)
  begin
    writeln("stdErr->" + error.toString());
    WriteToLog(error.toString());
  end);
end;

procedure TRagnarokServer.DoCmdDir(FileSystem: TW3NodeStorageDevice;
  Command: TWbCMDInfo; Socket: TNJWebSocketSocket;
  Session: TUserSession; Request: TQTXFileIORequest);
begin
  // Get the local filesystem
  var Filesys := GetFileSystem();

  // Check that its available
  if FileSys = nil then
  begin
    // Not available? OK, send an unsupported message
    SendUnSupported(Socket, Request.Ticket);
    exit;
  end;

  // Make sure its active
  if not FileSys.Active then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: filesystem not mounted error", false);
    exit;
  end;

  // Make sure user has logged in
  if Session.State <> asActive then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: user not authenticated error", false);
    exit;
  end;

  // Make sure user has read access
  if not (apRead in Session.Privileges) then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: user lacks read privilege error", false);
    exit;
  end;

  var LPath := IncludeTrailingPathDelimiter(Session.RootFolder);
  var LInitialPath := Command.ciParams.Join('');

  // All paths are relative to the userpath, so we brutally append
  if Command.ciParams.length > 0 then
  begin
    // Get the target path
    var temp :=  Command.ciParams.Join('');

    // prefixed with posix home moniker? Just remove it
    if LInitialPath.startswith('~/') then
      delete(Temp, 1, 2);

    // Build complete target path
    LPath := IncludeTrailingPathDelimiter(LPath) + temp;
  end;

  LPath := NodePathAPI.Normalize(LPath);

  var LServicePath := IncludeTrailingPathDelimiter('.\services');
      LServicePath := IncludeTrailingPathDelimiter(LServicePath) + 'directoryReader.js';

  ExecuteExternalJS([LServicePath, LPath], LInitialPath,
    procedure (Success: boolean; TagValue: variant; Data: string)
  begin
    case Success of
    true:
      begin
        var LResponse := TQTXFileIODirResponse.Create(Request.Ticket);
        try
          LResponse.LoadDirListFromString(Data);
          LResponse.Code := CNT_MESSAGE_CODE_OK;
          LResponse.Response := CNT_MESSAGE_TEXT_OK;
          LResponse.DirList.dlPath := TagValue;

          Socket.Send( LResponse.Serialize() );
        finally
          LResponse.free;
        end;
      end;
    false:
      begin
        SendInternalError(Socket, Request.Ticket,
          Format("Directory enumeration failed, %s", [Data]), false);
      end;
    end;
  end);
end;

procedure TRagnarokServer.DoFileIO(Sender: TObject;
    Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXClientMessage);
begin
  var LReq := TQTXFileIORequest(Request);

  // Get the local filesystem
  var Filesys := GetFileSystem();

  // Check that its available
  if FileSys = nil then
  begin
    // Not available? OK, send an unsupported message
    SendUnSupported(Socket, Request.Ticket);
    exit;
  end;

  // Make sure its active
  if not FileSys.Active then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: filesystem not mounted error", false);
    exit;
  end;

  // Make sure user has logged in
  if Session.State <> asActive then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: user not authenticated error", false);
    exit;
  end;

  // Make sure user has read access
  if not (apRead in Session.Privileges) then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed: user lacks read privilege error", false);
    exit;
  end;

  // Get the command text and validate
  var LText := LReq.Command.trim();
  if LText.Length < 1 then
  begin
    SendInternalError(Socket, Request.Ticket,
      "FileIO failed, command-text was empty", false);
    exit;
  end;

  //writeln("Received command-text:" + LText);

  // Make sure the text is CR terminated
  if not LText.EndsWith(#13) then
    LText += #13;

  // Create command-line parser
  var LParser := TWbCMDLineParser.Create;
  try
    // Parse the command
    LParser.Context.Buffer.LoadBufferText(LText);
    if not LParser.Parse() then
    begin
      // Text not recognized, return error but allow socket to be open
      SendInternalError(Socket, Request.Ticket, Format("FileIO failed, %s", [LParser.LastError]), false);
      exit;
    end;

    for var x := 0 to LParser.Count-1 do
    begin
      var cm := LParser.Items[x];

      case cm.ciCommand.ToLower() of
      'read':
        begin
          writeln("Executing the READ handler");
          DoCmdRead(Filesys, cm, socket, session, LReq);
        end;
      'dir':
        begin
          writeln("Executing the DIR handler");
          DoCmdDir(Filesys, cm, Socket, Session, LReq);
        end;
      'run':
        begin
          writeln("Executing the RUN handler");
          DoCmdRun(FileSys, cm, Socket, Session, LReq);
        end
      else
        begin
          SendInternalError(Socket, Request.Ticket, Format("FileIO failed, unknown command [%s]", [cm.ciCommand]), false);
          break;
        end;
      end;
    end;
  finally
    LParser.free;
  end;

end;



end.
