unit ragnarok.messages.base;

interface

uses
  System.Types,
  System.Objects,
  System.Types.Convert,
  System.Time,

  Ragnarok.JSON,

  System.Dictionaries,
  System.Memory.Buffer;


const
  CNT_CHANNEL_ID_DEFAULT  = '{5769FAF8-AD26-4941-9D4E-35E1DEDDBC82}';

type

  // Exceptions
  EQTXMessage = class(EW3Exception);

  //Forward declarations
  TQTXBaseMessage = class;

  TQTXRoute = class(TObject)
  private
    FOwner:     TQTXBaseMessage;
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); virtual;
    procedure   WriteObjData(const Root: TQTXJSONObject); virtual;
  public
    property    Parent: TQTXBaseMessage read FOwner;
    property    Channel: string;
    property    Source: string;
    property    Destination: string;
    property    TagValue: variant;
    procedure   Clear;
    constructor Create(const Message: TQTXBaseMessage);
  end;

  TQTXBaseMessage = class(TObject)
  private
    FBinary:    TBinaryData;
    FEnvelope:  TQTXJSONObject;
    FRouting:   TQTXRoute;
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); virtual;
    procedure   WriteObjData(const Root: TQTXJSONObject); virtual;
  public
    property    SendTime: TDateTime;
    property    Ticket: string;
    property    Attachment: TBinaryData read FBinary;
    property    Routing: TQTXRoute read FRouting;

    function    Serialize: string;
    procedure   Parse(const ObjectString: string);

    class function CreateTicket: string;
    class function QueryIdentifier(ObjectString: string): string;

    procedure   Clear; virtual;

    constructor Create; overload; virtual;
    constructor Create(const WithTicket: string); overload; virtual;
    destructor  Destroy; override;
  end;

  TQTXMessageBaseClass = class of TQTXBaseMessage;

  TQTXResponseMessage = class(TQTXBaseMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Code: integer;
    property    Response: string;
  end;

  TQTXErrorMessage = class(TQTXResponseMessage)
  end;


const
  CNT_ERR_CONSUME_ENTRYPOINT  = 'Failed to consume message, entrypoint "%s" not found';
  CNT_ERR_COMPOSE_ENTRYPOINT  = 'Failed to compose message, entrypoint "%s" not found';
  CNT_ERR_COMPOSE_WITHCLASS   = 'Failed to compose message, system threw exception %s with message: %s';
  CNT_ERR_COMPOSE_GENERAL     = 'Failed to compose message: %s';

const
  CNT_MESSAGE_CODE_OK     = 200;
  CNT_MESSAGE_TEXT_OK     = 'OK';

  CNT_MESSAGE_CODE_ERROR  = 500;
  CNT_MESSAGE_TEXT_ERROR  = 'Invalid or unsupported message [%s] error';

  CNT_MESSAGE_CODE_REJECTED = 501;
  CNT_MESSAGE_TEXT_REJECTED = 'The message was rejected by the server (%s)';


implementation


//#############################################################################
// TQTXResponseMessage
//#############################################################################

procedure TQTXResponseMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);

  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('response');
    if LTarget <> nil then
    begin
      LTarget.WriteOrAdd('code', Code);
      LTarget.WriteOrAdd('text', TString.EncodeBase64(Response));
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["response"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXResponseMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('response',  false);
    if LSource <> nil then
    begin
      Code := LSource.ReadInteger('code');
      Response := TString.DecodeBase64( LSource.ReadString('text') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["response"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXRoute
//#############################################################################

constructor TQTXRoute.Create(const Message: TQTXBaseMessage);
begin
  inherited Create();
  FOwner := Message;
end;

procedure TQTXRoute.Clear;
begin
  Source := '';
  Channel := CNT_CHANNEL_ID_DEFAULT;
  Destination := '';
  TagValue := unassigned;
end;

procedure TQTXRoute.ReadObjData(const Root: TQTXJSONObject);
begin
  Channel := TString.DecodeBase64( Root.readString('channel') );
  Source := TString.DecodeBase64( Root.readstring('source') );
  Destination := TString.DecodeBase64( Root.readString('destination') );

  if Root.Locate('tagvalue', false) <> nil then
    TagValue := Root.ReadVariant('tagvalue');
end;

procedure TQTXRoute.WriteObjData(const Root: TQTXJSONObject);
begin
  Root.WriteOrAdd('channel', TString.EncodeBase64(Channel) );
  Root.WriteOrAdd('source', TString.EncodeBase64(Source) );
  Root.WriteOrAdd('destination', TString.EncodeBase64(Destination) );
  Root.WriteVariant('tagvalue', TagValue);
end;

//#############################################################################
// TQTXBaseMessage
//#############################################################################

constructor TQTXBaseMessage.Create;
begin
  inherited Create();
  FBinary := TBinaryData.Create();
  FEnvelope := TQTXJSONObject.Create();
  FRouting := TQTXRoute.Create(self);
end;

constructor TQTXBaseMessage.Create(const WithTicket: string);
begin
  self.Create();
  Ticket := WithTicket;
end;

destructor TQTXBaseMessage.Destroy;
begin
  FBinary.free;
  FEnvelope.free;
  FRouting.free;
  inherited;
end;

procedure TQTXBaseMessage.Clear;
begin
  FBinary.Release();
  FEnvelope.Clear();
  FRouting.Clear();
end;

class function TQTXBaseMessage.CreateTicket: string;
begin
  result := TString.CreateGUID();
end;

class function TQTXBaseMessage.QueryIdentifier(ObjectString: string): string;
begin
  var LEnvelope := TQTXJSONObject.Create;
  try
    try
      LEnvelope.FromJSON(ObjectString);

      if LEnvelope.Exists('identifier') then
        result := LEnvelope.ReadString('identifier');
    except
      exit;
    end;
  finally
    LEnvelope.free;
  end;
end;

function TQTXBaseMessage.Serialize: string;
begin
  WriteObjData(FEnvelope);
  result  := FEnvelope.ToJSON(4);
end;

procedure TQTXBaseMessage.Parse(const ObjectString: string);
begin
  FEnvelope.FromJSON(ObjectString);
  ReadObjData(FEnvelope);
end;

procedure TQTXBaseMessage.ReadObjData(const Root: TQTXJSONObject);
begin
  if Root.ReadString('identifier') = classname then
  begin
    Ticket := Root.ReadString('ticket');

    var LRoute := Root.locate('routing', false);
    if LRoute <> nil then
      FRouting.ReadObjData(LRoute);

    var LSource := Root.Locate(ClassName, false);
    if LSource <> nil then
    begin
      SendTime := LSource.ReadFloat('sendtime');
      Attachment.FromBase64( LSource.Read('attachment') );
    end;
  end else
  raise EW3Exception.CreateFmt("Expected identifier of [%s] error", [ClassName]);
end;

procedure TQTXBaseMessage.WriteObjData(const Root: TQTXJSONObject);
begin
  Root.WriteOrAdd('identifier', ClassName);
  Root.WriteOrAdd('ticket', Ticket);

  var LRoute := Root.branch('routing');
  FRouting.WriteObjData(LRoute);

  SendTime := now;
  var LTarget := Root.Branch(ClassName);
  LTarget.WriteOrAdd('sendtime', SendTime);
  LTarget.WriteOrAdd('attachment', FBinary.ToBase64() );
end;

end.

