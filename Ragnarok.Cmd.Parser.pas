//#############################################################################
// ______                                  _
// | ___ \ Smart Mobile Studio            | |
// | |_/ /__ _  __ _ _ __   __ _ _ __ ___ | | __
// |    // _` |/ _` | '_ \ / _` | '__/ _ \| |/ /
// | |\ \ (_| | (_| | | | | (_| | | | (_) |   <
// \_| \_\__,_|\__, |_| |_|\__,_|_|  \___/|_|\_\
//              __/ |
//             |___/          Powered by Node.js
//
//#############################################################################

unit Ragnarok.Cmd.Parser;

interface

uses
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Text.Parser,
  System.NameValuePairs,
  SmartNJ.System,
  SmartNJ.Streams,
  SmartNJ.Application;

type

  TWbCMDInfo = record
    ciCommand:  String;
    ciParams:   TStrArray;
  end;
  TWbCMDInfoArray = array of TWbCMDInfo;

  TWbCMDLineParser = class(TCustomParser)
  private
    FItems:     TWbCMDInfoArray;
  public
    property    Items[index: integer]: TWbCMDInfo read (FItems[index]);
    property    Count: integer read (FItems.Count);
    procedure   Clear; virtual;
    function    Parse: boolean; override;
    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
  end;

implementation

//#####################################################################
// TWbCMDLineParser
//#####################################################################

constructor TWbCMDLineParser.Create;
begin
  inherited Create( TParserContext.Create("") );
  ErrorOptions.AutoResetError := true;
  ErrorOptions.AutoWriteToConsole := true;
  ErrorOptions.ThrowExceptions := false;
end;

destructor TWbCMDLineParser.Destroy;
begin
  if FItems.Count > 0 then
    Clear();
  inherited;
end;

procedure TWbCMDLineParser.Clear;
begin
  FItems.Clear();
end;

function TWbCMDLineParser.Parse: boolean;
begin
  if ErrorOptions.AutoResetError then
    ClearLastError();

  var LBuffer := Context.Buffer;

  if not LBuffer.Empty then
  begin
    LBuffer.first();

    //writeln("About to parse:" + LBuffer.CacheData);

    repeat
      var LCMD := '';
      var LParams: TStrArray = [];

      LBuffer.ConsumeJunk();
      if LBuffer.EOF then
        break;

      // Get command
      if not LBuffer.ReadWord(LCMD) then
      begin
        //writeln("Failed to read command");
        SetLastError('Syntax error, failed to read command error');
        break;
      end;

      var Rec: TWbCMDInfo;
      Rec.ciCommand := LCMD;

      // Skip potential spaces
      LBuffer.ConsumeJunk();

      // Check that this is not the end
      if LBuffer.EOF or LBuffer.CrLf then
      begin
        FItems.add(Rec);
        break;
      end;

      if not LBuffer.ReadCommaList(LParams) then
      begin
        //writeln("Could not read the list at all");
        break;
      end;

      //writeln("We read the list just fine:");

      // Copy over params
      for var item in LParams do
      begin
        //writeln("Param:" + Item);
        Rec.ciParams.add(Item);
      end;

      // Add command-element to our items
      FItems.add(rec);

      if not LBuffer.Next() then
        break;

    until LBuffer.EOF;

    result := not Failed;

  end else
  SetLastError('Parsing failed, buffer is empty error');
end;


end.
