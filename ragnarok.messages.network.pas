unit ragnarok.messages.network;

interface

uses
  System.Types,
  System.Objects,
  System.Types.Convert,
  System.Time,
  System.Device.Storage,
  System.Dictionaries,

  ragnarok.json,
  ragnarok.messages.base;




type

  TQTXClientMessage = class(TQTXBaseMessage)
  end;

  // This class was implemented here earliner, but
  // its been moved into Messages::Base since its fairly universal
  TQTXServerMessage = TQTXResponseMessage;

  // Since errors and response messages are the same,
  // this class has been redeclared in Messages::Base as TQTXErrorMessage
  TQTXServerError = TQTXErrorMessage;

  TQTXChannelOperation = (coOpen, coClose, coQuery);

  TQTXRelayRequest = class(TQTXClientMessage)
  private
    FRelay:     TQTXJSONObject;
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    function    GetMessage: TQTXJSONObject;
    function    GetMessageRoute: TQTXRoute;
    procedure   SetMessage(const ThisMessage: TQTXJSONObject);
  end;

  TQTXLoginRequest = class(TQTXClientMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Username: string;
    property    Password: string;
  end;

  TQTXLoginResponse = class(TQTXServerMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Username: string;
    property    SessionID: string;
  end;

  TQTXFileIORequest = class(TQTXClientMessage)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Command: string;
    property    SessionId: string;
  end;

  TQTXFileIOResponse = class(TQTXServerMessage)
  end;

  TQTXFileIODirResponse = class(TQTXFileIOResponse)
  private
    FDirList:   TNJFileItemList;
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    DirList: TNJFileItemList read FDirList;
    procedure   LoadDirListFromString(const Data: string);
    constructor Create; override;
  end;

  TQTXFileIOLoadResponse = class(TQTXFileIOResponse)
  protected
    procedure   ReadObjData(const Root: TQTXJSONObject); override;
    procedure   WriteObjData(const Root: TQTXJSONObject); override;
  public
    property    Filename: string;
  end;

implementation

//#############################################################################
// TQTXRelayRequest
//#############################################################################

function TQTXRelayRequest.GetMessage: TQTXJSONObject;
begin
  result := FRelay;
end;

function TQTXRelayRequest.GetMessageRoute: TQTXRoute;
begin
  if FRelay <> nil then
  begin
    var LTemp := TQTXBaseMessage.Create();
    LTemp.Parse(FRelay.ToJSON());
    result := LTemp.Routing;
  end;
end;

procedure TQTXRelayRequest.SetMessage(const ThisMessage: TQTXJSONObject);
begin
  FRelay := ThisMessage;
end;

procedure TQTXRelayRequest.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('relay');
    if LTarget <> nil then
    begin
      var LEnvelope := LTarget.Branch('envelope');
      if FRelay <> nil then
        LEnvelope.WriteOrAdd('message', FRelay);

    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["relay"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXRelayRequest.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('relay',  false);
    if LSource <> nil then
    begin
      var LEnvelope := LSource.Locate('envelope', false);
      if LEnvelope <> nil then
        FRelay := LEnvelope.Locate('message', false)
      else
        raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ['envelope'])

    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ['relay']);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXFileIOLoadResponse
//#############################################################################

procedure TQTXFileIOLoadResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
    LParent.WriteOrAdd('filename', TString.EncodeBase64(Filename))
  else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXFileIOLoadResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    if LParent.Exists('filename') then
      Filename := TString.decodeBase64( LParent.ReadString('filename') )
    else
      raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["directorylist"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXFileIODirResponse
//#############################################################################

constructor TQTXFileIODirResponse.Create;
begin
  inherited;
  FDirList := new TNJFileItemList();
end;

procedure TQTXFileIODirResponse.LoadDirListFromString(const Data: string);
begin
  var LTemp := JSON.Parse(Data);
  asm
    (@self.FDirList) = @LTemp;
  end;
end;

procedure TQTXFileIODirResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
    LParent.WriteOrAdd('filesystem', FDirList)
  else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXFileIODirResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    if LParent.Exists('filesystem') then
      FDirList := TNJFileItemList( TVariant.AsObject( LParent.Read('filesystem') ) )
    else
      raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["filesystem"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXFileIORequest
//#############################################################################

procedure TQTXFileIORequest.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('filesystem');
    if LTarget <> nil then
    begin
      LTarget.WriteOrAdd('command', TString.EncodeBase64(Command) );
      LTarget.WriteOrAdd('sessionid', TString.EncodeBase64(SessionId));
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["filesystem"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXFileIORequest.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('filesystem',  false);
    if LSource <> nil then
    begin
      Command := TString.DecodeBase64( LSource.ReadString('command') );
      SessionId := TString.DecodeBase64( LSource.ReadString('sessionid') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ['filesystem']);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXLoginResponse
//#############################################################################

procedure TQTXLoginResponse.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('session');
    if LTarget <> nil then
    begin
      LTarget.WriteOrAdd('username', TString.EncodeBase64(Username) );
      LTarget.WriteOrAdd('sessionid', TString.EncodeBase64(SessionID));
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["session"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXLoginResponse.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('session',  false);
    if LSource <> nil then
    begin
      Username := TString.DecodeBase64( LSource.ReadString('username') );
      SessionID := TString.DecodeBase64( LSource.ReadString('sessionid') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ['session']);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;

//#############################################################################
// TQTXLoginRequest
//#############################################################################

procedure TQTXLoginRequest.WriteObjData(const Root: TQTXJSONObject);
begin
  inherited WriteObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LTarget := LParent.Branch('credentials');
    if LTarget <> nil then
    begin
      LTarget.WriteOrAdd('username', TString.EncodeBase64(Username) );
      LTarget.WriteOrAdd('password', TString.EncodeBase64(Password));
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, ["credentials"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_COMPOSE_ENTRYPOINT, [ClassName]);
end;

procedure TQTXLoginRequest.ReadObjData(const Root: TQTXJSONObject);
begin
  inherited ReadObjData(Root);
  var LParent := Root.Locate(ClassName, false);
  if LParent <> nil then
  begin
    var LSource := LParent.Locate('credentials',  false);
    if LSource <> nil then
    begin
      Username := TString.DecodeBase64( LSource.ReadString('username') );
      Password := TString.DecodeBase64( LSource.ReadString('password') );
    end else
    raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, ["credentials"]);
  end else
  raise EQTXMessage.CreateFmt(CNT_ERR_CONSUME_ENTRYPOINT, [ClassName]);
end;


end.
