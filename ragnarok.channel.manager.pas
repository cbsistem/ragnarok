unit ragnarok.channel.manager;

interface

uses
  System.Types,
  System.Objects,
  System.Types.Convert,
  System.dictionaries,
  System.Time,

  Ragnarok.JSON,

  System.Dictionaries,
  System.Memory.Buffer;

type

  // Forward declarations
  TChannel              = class;
  TChannelManager       = class;
  TMessageSubscription  = class;
  TMessageSubscribers   = class;

  // Exceptions
  EChannelError = class(EW3Exception);

  // This type is for the channel-subscription callback.
  // Whenever a message is sent on a particular channel, all subscribers
  // are notified via a callback. The activity param tells the subscriber
  // what exactly is taking place.
  TSubscriptionActivity = (
    saDispatch,   // A message is available
    saClose,      // The channel is closing
    saRemoved     // The subscription is being removed
    );

  // This is the subscription callback
  // Multiple handlers can "listen" to a channel and be notified
  // about the arrival of a message. The activity denotes if
  // a message is delivered, or a channel is closing.
  // Note: to bind a callback to an object (socket for example),
  // pass it along as the "TagValue" parameter when you register the CB.
  // That TagValue will be passed along in the callback
  TSubScriberCallback = procedure (TagValue: variant; Activity: TSubscriptionActivity; Channel: TChannel; Message: TObject);

  // Simple object to handle
  TMessageSubscription = class(TObject)
  public
    property    Identifier: string;
    property    TagValue: variant;
    property    Callback: TSubScriberCallback;
  end;

  TMessageSubscribers = class(TObject)
  private
    FLUT:       TW3ObjDictionary;
    FItems:     array of TMessageSubscription;
    FChannel:   TChannel;
  public
    property    &Empty: boolean read (FItems.length > 0);
    function    AddSubscription(TagValue: variant; const CB: TSubScriberCallback): string;
    procedure   RemoveSubscription(Identifier: string; Silent: boolean);

    procedure   Dispatch(const Message: TObject);

    procedure   Close;

    procedure   Clear(const Silent: boolean); overload;
    procedure   Clear; overload;
    constructor Create(const Channel: TChannel); virtual;
    destructor  Destroy; override;
  end;

  TChannel = class(TObject)
  private
    FId:        string;
    FManager:   TChannelManager;
    FItems:     TMessageSubscribers;
  protected
    function    GetChannelId: string; virtual;
  public
    property    Identifier: string read FId;
    property    Subscriptions: TMessageSubscribers read FItems;
    property    TagValue: variant;

    procedure   Dispatch(const Message: TObject);

    procedure   Close;

    constructor Create(const Manager: TChannelManager; ChannelName: string); virtual;
    destructor  Destroy; override;
  end;

  TChannelManager = class(TObject)
  private
    FLUT:       TW3ObjDictionary;
  public
    property    Channels: TW3ObjDictionary read FLUT;

    function    CreateChannel(Identifier: string; TagValue: variant): TChannel; overload;
    function    CreateChannel(TagValue: variant): TChannel; overload;
    function    GetChannelById(Identifier: string): TChannel;
    procedure   CloseChannel(Identifier: string);

    procedure   CloseAll;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

//#############################################################################
// TChannelManager
//#############################################################################

constructor TChannelManager.Create;
begin
  inherited Create;
  FLUT := TW3ObjDictionary.Create();
end;

destructor TChannelManager.Destroy;
begin
  FLUT.free;
  inherited;
end;

procedure TChannelManager.CloseAll;
begin
  FLut.ForEach(
    function (ItemKey: string; var KeyData: variant): TEnumResult
    begin
      CloseChannel(ItemKey);
      result := TEnumResult.erContinue;
    end);
end;

procedure TChannelManager.CloseChannel(Identifier: string);
begin
  Identifier := Identifier.trim();
  var LChannel := if FLut.Contains(Identifier) then TChannel(FLut.Values[Identifier]) else nil;
  if LChannel <> nil then
  begin
    // Close all subscruptions
    LChannel.Subscriptions.Close();
    LChannel.Close();

    // Remove channel
    FLut.Delete(Identifier);

    // Dispose of channel object
    LChannel.free;
  end else
  raise EChannelError.CreateFmt('Channel found found error [%s]', [Identifier]);
end;

function TChannelManager.CreateChannel(Identifier: string; TagValue: variant): TChannel;
begin
  Identifier := Identifier.trim();
  if FLut.Contains(Identifier) then
    raise EChannelError.CreateFmt('Failed to create channel [%s], route already exists error', [Identifier]);

  result := TChannel.Create(self, Identifier);
  FLut.Values[result.Identifier] := result;
  result.TagValue := TagValue;
end;

function TChannelManager.CreateChannel(TagValue: variant): TChannel;
begin
  result := TChannel.Create(self, '');
  FLut.Values[result.Identifier] := result;
  result.TagValue := TagValue;
end;

function TChannelManager.GetChannelById(Identifier: string): TChannel;
begin
  result := if FLut.Contains(Identifier) then TChannel(FLut.Values[Identifier]) else nil;
end;

//#############################################################################
// TChannel
//#############################################################################

constructor TChannel.Create(const Manager: TChannelManager; ChannelName: string);
begin
  inherited Create;
  FItems := TMessageSubscribers.Create(self);
  FManager := Manager;

  ChannelName := ChannelName.trim();
  FId := if ChannelName.length > 0 then ChannelName else GetChannelId();
end;

destructor TChannel.Destroy;
begin
  try
    Close();
  finally
    FItems.free;
  end;
  inherited;
end;

procedure TChannel.Close;
begin
  try
    if not FItems.&Empty then
    begin
      try
        FItems.Close();
      except
        // sink exception
      end;
    end;
  finally
    FItems.Clear();
  end;
end;

function TChannel.GetChannelId: string;
begin
  // Create a unique identifier if the channel
  // is not created with a spesific name
  result := TString.CreateGUID();
end;

procedure TChannel.Dispatch(const Message: TObject);
begin
  if Message <> nil then
  begin
    try
      FItems.Dispatch(Message);
    except
      on e: exception do
      raise EChannelError.CreateFmt('Failed to dispatch message on channel [%s]: %s', [FId, e.message]);
    end;
  end else
  raise EChannelError.Create('Failed to dispatch message, instance was nil error');
end;


//#############################################################################
// TMessageSubscribers
//#############################################################################

constructor TMessageSubscribers.Create(const Channel: TChannel);
begin
  inherited Create;
  FChannel := Channel;
  FLut := TW3ObjDictionary.Create;
end;

destructor TMessageSubscribers.Destroy;
begin
  if FItems.Count > 0 then
    Clear(false);
  FLut.free;
  inherited;
end;

procedure TMessageSubscribers.Close;
begin
  if FItems.Count > 0 then
  begin
    try
      for var LObj in FItems do
      begin
        if assigned(LObj.Callback) then
          LObj.Callback(LObj.TagValue, saClose, FChannel, nil);
      end;
    finally
      Clear();
    end;
  end;
end;

procedure TMessageSubscribers.Clear;
begin
  if FItems.Count > 0 then
    Clear(false);
end;

procedure TMessageSubscribers.Clear(const Silent: boolean);
begin
  if FItems.Count > 0 then
  begin
    try
      for var LItem in FItems do
      begin
        RemoveSubscription(LItem.Identifier, Silent);
      end;
    finally
      FItems.Clear();
      FLut.Clear();
    end;
  end;
end;

function TMessageSubscribers.AddSubscription(TagValue: variant; const CB: TSubScriberCallback): string;
begin
  result := TString.CreateGUID();
  var LObj := TMessageSubscription.Create();
  LObj.Identifier := result;
  LObj.TagValue := TagValue;
  FLut.Values[result] := LObj;
  FItems.add(LObj);
end;

procedure TMessageSubscribers.RemoveSubscription(Identifier: string; Silent: boolean);
begin
  if FLut.Contains(Identifier) then
  begin
    // Extract object
    var LObj := TMessageSubscription( FLUT.Values[Identifier] );

    // Remove from lookup table
    FLut.Delete(Identifier);

    // Remove from linear array
    for var x:= FItems.low() to FItems.High() do
    begin
      if FItems[x].Identifier = Identifier then
      begin
        FItems.Delete(x, 1);
        break;
      end;
    end;

    // Fire removal callback, unless silenced
    if not Silent then
    begin
      if assigned(LObj.Callback) then
        LObj.Callback(LObj.TagValue, saRemoved, FChannel, nil);
    end;
  end;
end;

procedure TMessageSubscribers.Dispatch(const Message: TObject);
begin
  if Message <> nil then
  begin
    if FItems.Count > 0 then
    begin
      for var LObj in FItems do
      begin
        try
          if assigned(LObj.Callback) then
            LObj.Callback(LObj.TagValue, saDispatch, FChannel, Message);
        except
        end;
      end;
    end;
  end else
  raise EChannelError.Create('Failed to dispatch message, instance was nil error');
end;

end.
